# changelog getInfo

tags utilisés: added  changed  cosmetic  deprecated  fixed  new rewriting  removed  syncro  security
date au format: YYYY-MM-DD

## [ Unreleased ]

à synchro: 
fs_script*, f__basedirname, 
f__user

## [ 4.21.1 ] - 2019.12.15

* rewriting f__wget_test
* added fi_system, SysVinit & openRc
* fixed option --help manquante
* fixed fi_audio, suppression erreur si manque FS virtuel module

## [ 4.20.0 ] - 2019.05.5

* fixed: figet_lspci, en cas d'absence d'un matériel la liste des modules est vide au lieu d'afficher tous les modules
* cosmetic: fi_net, en cas  d'absence d'un matériel, affichage 'vide'

## [ 4.19.0 ] - 2019.05.4

* fixed: bash v5 utilisable (v4+)

## [ 4.18.2 ] - 2018.08.01

* cosmetic: f__basedirname, f__user, fi_dmesg, fi_journal, fi_ssid, fi_usb, figet_distrib, fipaste, prg_init, prg_menu
* cosmetic: fscript_install, fscript_remove
* rewriting : figet_disk, plus d'utilisation de tail
* added: fi_reseau, conf dhcpcd
* added: fi_serial, cpu serial (raspberry)
* added: figet_wm, version enlightenment

## [ 4.17.0 ] - 2018.07.05

* added: avertissement mise à jour update-pciids
* added: fi_gpu, affichages connectés si 2 ou plus (bug nvidia possible)
* added: fi_gpu, module nvidia si présent
* added: load average: nb de process
* changed: fi_system_rc, plus de recalcul loadaverage
* fixed: fi_gpu, figet_screen, affichage en ssh
* fixed: figet_cpu, nb de cpu sur raspberry
* fixed: figet_cpu_uarch, détection fabricant
* fixed: fi_system, loadaverage, figet_cpu, nb de cores

## [ 4.16.0 ] - 2018.07.03

* cosmetic: fi_touchpad
* added: f__basedirname, f__die, f__sort_uniq
* added: fi_conf, fedora SELinux status 
* rewriting: basename externe out, dirname externe out
* rewriting: sort out & uniq out, remplacé par f__sort_uniq: 
		fi_cpu, fi_audio, fi_gpu_openGl, fi_pkg_apt, fi_pkg_apt, fi_usb, figet_hw, figet_lspci, figet_modules, figet_shell
* rewriting: f__requis, f__sort_uniq
* rewriting: f__wcv -> f__wc
* fixed: fi_locale, pas de d'affichage erreur si timedatectl en erreur 
* fixed: f__requis, certains packages ou commandes peuvent ne pas sortir en manquant
* fixed: fi_gpu_openGl, affichage sous ssh
* fixed: fi_gpu, glxinfo ssh
* fixed: figet_ip, détermination interface par défaut en ipv6, détection link

## [ 4.15.0 ] - 2018.06.27

* cosmetic: fi_audio, modules alsa, tri unique
* rewriting: f__wget_test, fi_gpu, figet_batt, figet_distrib, xargs out
* rewriting: figet_disk, fi_disk, passage en array
* added: f__clear_ligne

## [ 4.14.0 ] - 2018.06.26

* cosmetic: effacement notice sudo dans fi_journal
* added: notice sudo dans fi_dmesg
* fixed: fi_locale, traitement erreur RTC avec timedatectl
* fixed: figet_cpu, sortie fréquence nominale
* fixed: figet_cpu, nb proc, nb de threads "aléatoire"

## [ 4.13.0 ] - 2018.06.26

* cosmetic: prompt dans f_affichage, fi_audio
* changed: l'installation ne crée que le lanceur gfetch, --irc inscrit gfetch dans bashrc, --rrc désinscrit gfetch dans bashrc
* rewriting: suppression grep, f_search_ko, figet_lspci
* fixed: f_lifting, suppression _globale_ pour motifs oubliés
* fixed: f_search_ko, Échec du pairage de ( ou de \(...
* fixed: modules kernel, ctrl+V intempestif

## [ 4.11.0 ] - 2018.06.25

* fixed: figet_cpu_uarch pour certains procs amd

## [ 4.10.0 ] - 2018.06.24

* cosmetic: $serverX, renommage $ENV_noDISPLAY
* added: f__ls
* added: f__sec2day
* added: merge 04/2018 oublié :/
* added: adaptation ARM, raspberry
    * fi_system
    * figet_cpu
    * figet_cpu_uarch
    * figet_lspci
    * figet_dmi, dtb au lieu de dmi
    * fi_system
* added: figet_dtb
* added: extraction bugs cpu, figet_cpu, figet_cpu
* rewriting: fi_system, fi_system_rc révision "uptime" (lastboot en jours, heures...)
* rewriting: gfetch, fi_system_rc, quantité d'affichage
* rewriting: fi_usb, suppression emploi IFS/for, renommage $ls -> lsusb_
* rewriting: figet_shell, remplacement for par read
* rewriting: initialisation PATH et IFS, suppression IFS_INI (unset): 
* rewriting: f_search_k, suppression emploi IFS/for
* fixed: lastboot erroné sur longue période, fi_system
* fixed: f_grep_file, affichage vide


## [ 4.9.1 ] - 2018.06.20

* cosmetic: divers
* fixed: f_help
* fixed: traitement option avec durée paste spéciale
* rewriting: fipaste_pastery

## [ 4.8.1 ] - 2018.06.18

* cosmetic: lint, fi_pkg_apt, figet_disk
* cosmetic: f_n_host, f_n_dig
* syncro: f__random
* changed: syncro & renommage f__cnx -> f_n_cnx
* changed: option -t x durée du paste
* rewriting: f_ip_validate, syncro
* rewriting: f_n_dig, f_n_host
* rewriting: f_ip_pub, prise en charge f_n_host f_n_dig
* added: f_n_host
* added: compatibily for ArchLinux, drill instead dig, f_n_dig, f_ip_dns, f_ip_master, f_ip_pub

## [ 4.7.0 ] - 2018.06.16

* cosmetic: fi_dmesg, fi_journal, fi_bluez, fi_system, figet_hw, figet_lspci, figet_mem, essentiellement shellcheck
* added: drill for compatibility with archlinux, f__dig
* added: option --dev pour chargement version dev (si existante)
* rewriting: traitement options d'appel

## [ 4.6.0 ] - 2018.06.16

* fixed: f__cnx server ipv6, f_ip_pub
* fixed: ip publiques (--ip)
* changed: plus de détection ip avec telnet ou nc (plus de services connus), figet_ip_pub -> f_ip_pub

## [ 4.5.0 ] - 2018.04.14

* syncro: composants, fscript_update avec ses parametres d'appel, renommage f__test_cnx -> f__cnx
* cosmetic: affichage & help, lint
* rewriting: f__sudo, plus de multiples tentatives pour su, plus simple et fix potentiel stderr non visible
* rewriting: f__dialog_oui_non
* fixed: f__log
* fixed: figet_ip, extraction lien local

## [ 4.4.1 ] - 2018.04.08

* fixed: archlinux, publi urgente
* added: crypttab (fi-disk)
* rewriting: figet_disk, fi_disk, affichage
* rewriting: fi_system_rc, 
* fixed: contournement voidlinux (mawk par erreur)
* fixed: fi_gpu, variable non quotée sur test

## [ 4.3.0 ] - 2018.03.08

* rewriting: shellcheck
* cosmetic: suppression f__which, quelques optimisations vitesse
* rewriting: f_affichage
* rewriting: figet_disk, fi_disk
* rewriting: figet_ip_pub, +f__test_cnx
* rewriting: f_grep_file (fstab)
* fixed: fi_pkg_x, figet_distrib

## [ 4.2.1 ] - 2018.03.04

* fixed: figet_ip_pub, ubuntu 16.04
* cosmetic:

## [ 4.1.0 ] - 2018.03.03

* cosmetic:

## [ 4.1.0 ] - 2018.03.03

* added: lanceur gfetch (oublié publié)

## [ 4.0.0 ] - 2018.03.02

* added: option rc, affichage résumé ala neofetch, installation incluse dans l'install standard
* rewriting: figet_ip, ipv4
* fixed: figet_de, version xfce
* fixed: optype

## [ 3.28.0 ] - 2018.03.01

* added: fi_system, alerte SLiM+systemd
* rewriting: f__color, prg_init (opions bash)
* fixed: fi_bluez, vitesse (erreur de commande)

## [ 3.27.0 ] - 2018.02.28

* rewriting: fi_cpu, fi_pkg_apt
* fixed: détection ssh
* fixed: fi_local en ssh et ssh X
* fixed: f__wcv -l

## [ 3.26.0 ] - 2018.02.26

* rewriting: f__trim (fi_pkg_apt, paquets épinglés)
* rewriting: figet_disk

## [ 3.25.0 ] - 2018.02.25

* rewriting: suppression traces test mawk
* rewriting: figet_de, contrôle cinnamon, xfce et mieux sur kde
* fixed: figet_hw, développement incorrect chemin si vide sous vbox

## [ 3.24.0 ] - 2018.02.24

* rewriting: général, empilement de sous-shell et variables HERE
* rewriting: figet_screen
* rewriting: f__sudo, fi_journal, explication demande pass
* fixed: figet_hw, extraction fan

## [ 3.23.0 ] - 2018.02.23

* added: pager rapport si pas de $DISPLAY
* rewriting: figet_de,, révision
* fixed: --debug, permissions
* fixed: figet_dm, erreur sous noX, display-manager.service
* fixed: f__sudo, nb tentatives idem entre su et sudo
* fixed: f__sudo &2>&1, demande pass visible quand --debug

## [ 3.22.0 ] - 2018.02.22

* added: version lightdm, xfwm, xfce
* added: adaptation ubuntu, upstart, unity version, compiz version, mir
* added: datage report debug
* fixed: date installation
* fixed: général, ubuntu 16.04, complètement OUT, presque aussi cata que voidLinux. version bash? buggée ou osbsolescente?
* fixed: fi_journal, obsolescence systemd buntu 16.04 

## [ 3.21.0 ] - 2018.02.21

* added: fi_conf, AppArmor
* added: fi_mem, type de swap
* rewriting: affichage fi_gpu
* rewriting: figet_dm, extraction gdm3 / gdm
* rewriting: figet_wm, gnome3/ubuntu 17.10
* rewriting: fi_efi, aucun affichage si mbr
* fixed: mineur, figet_ip & fi_reseau quand ipv6 ou ipv4 absent
* fixed: figet_lspci, condition slot absent
* fixed: mineur, f_dspl, affiche fichier nofile

## [ 3.20.0 ] - 2018.02.20

* added: option --debug log erreurs de script, exporté avec rapport principal
* rewriting: figet_wm
* rewriting: figet_cpu, affichage virtualbox
* fixed: fi_system, affichage touchpad=0, méthode extraction init 

## [ 3.19.0 ] - 2018.02.19

* added: fi_pkg_apt, paquets épinglés par pinning, résultat effectif
* rewriting: général, ps -ef
* rewriting: fi_reseau, manager de réseau
* rewriting: mineur, figet_cpu, affichage nom cpu
* rewriting: figet_wm
* fixed: f_dspl, affichage si vide
* fixed: fi_touchpad, qte numérique sur zéro touchpad

## [ 3.18.3 ] - 2018.02.14

* added: figet_de, ajout panels
* rewriting: fi_touchpad, titrage & motif grep devices
* fixed: fi_touchpad, affichages effacés par erreur dans 3.14

## [ 3.17.2 ] - 2018.02.13

* rewriting: figet_wm (+figet_wm_annexe), ajout nouveau DM, objectif DWM, remodelage scission figet_wm & figet_compositor
* rewriting: général, protection erreur si xprop non dispo
* rewriting: fi_system, figet_dm, plus de dm actif
* fixed: fi_system, nb_desktop avec dwm
* fixed: figet_dm, fix extraction
* fixed: fi_audio, erreur quand aplay ou arecord manquants
* fixed: prg_init, tentative prévention exécution avec un awk à la voidlinux

## [ 3.16.1 ] - 2018.02.11

* syncro: f__color

## [ 3.16.0 ] - 2018.02.10

* added: fi_reseau: ipv6, netplan ubuntu
* added: fi_system, qte desktop
* added: fi_conf(), /etc/default/grub.d/*.cfg
* added: fi_audio, devices alsa
* rewriting: fi_gpu, extractions version xorg
* rewriting: fi_reseau, affichage ipv6 dynamique, ipv6 dépréciées
* rewriting: figet_ip, ipv6, traitemenent adresses dynamiques
* fixed: fi_bluez, affichage modules manquants (dev non enlevé)

## [ 3.14.0 ] - 2018.02.09

* rewriting: fi_gpu, fi_audio, fi_net (mod figet_lspci)
* fixed: fi_gpu, affichage cmd lspci -nnv, mod figet_lspci
* fixed: fi_touchpad, affichage matériel
* fixed: fi_system, affichage touchpad si > 1
* fixed: figet_lspci si optirun

## [ 3.13.0 ] - 2018.02.07

* rewriting: fi_gpu
* rewriting: général, pas de string vide pour gettext
* fixed: fi_gpu, affichage version serveur xorg & lancement optirun
* fixed: figet_screen, résolution avec xdpyinfo quand plusieurs écrans, priorité à xrandr

## [ 3.12.0 ] - 2018.02.01

* added: f__dialog_oui_non, option clear, fix timeout
* fixed: analyse réseau, squizzée par erreur

## [ 3.11.2 ] - 2018.01.28

* fixed: fonctionnement avec mawk, fi_pkg_apt, figet_de, fi_cpu

## [ 3.10.0 ] - 2018.01.27

* rewriting: fi_pkg_apt, affichage awk , alerte erreur apt, affichage mineur
* fixed: affichage fi_efi
* fixed: fi_pkg_apt, affichage sources ignorées
* fixed: f_grep_file, prise en charge .sources
* fixed: certains fichiers preferences non affichés
* fixed: fichiers ignorés pour sources & preferences (nouveau: f_policy)

## [ 3.9.0 ] - 2018.01.26

* rewriting: test avec mawk, f__user, figet_ip, f__unit_human
* rewriting: plus d'imposition gawk only, prg_init
* rewriting: mineur, fscript_cronAnacron fscript_install fscript_remove fscript_update
* rewriting: f__requis, commandes alternatives possible (gawk|mawk), mawk installé par défaut sur debian :(
* rewriting: f__requis_deb issu de f__requis, fi_pkg_apt
* rewriting: fichiers temporaires fi_dmesg, fi_journal
* fixed: expression mawk, fi_system_analyse
* fixed: f__sudo, extraction nb tentatives avec getInfo

## [ 3.7.0 ] - 2018.01.24

* added: cumul options (opérations) possibles pour la plupart des opérations
* rewriting: invocation f__sudo dans traitement options, plus confortable si su & _all_
* rewriting: f__wget_test
* rewriting: f_sudo abandonné dans fscript_install et fscript_remove, au profit appel au traitement général des options
* rewriting: f_help, f_affichage
* rewriting: général wget_log: fscript_get_version, fscript_update
* removed: liens et tests frama.link 

## [ 3.6.0 ] - 2018.01.14

* rewriting: f_sudo, format nombre de tentatives et options appel possibles > 1

## [ 3.5.0 ] - 2018.01.12

* fixed: correction commentaire fscript_get_version

## [ 3.4.0 ] - 2017.12.31

* cosmetic: fi_pkg_apt
* fixed: mineur, oubli user_agent

## [ 3.3.0 ] - 2017.12.29

* syncro: composants

## [ 3.2.0 ] - 2017.12.26

* rewriting: f__info, option combinée raw:log

## [ 3.1.0 ] - 2017.12.24

* syncro: nouveau composants scripts
* rewriting: fi_ssid, : f_sudo
* rewriting: figet_ip_pub, wget, définition logs, pour cause de bug wget? sur testing
* fixed: f__wget_test, incompatible avec redirection logs

## [ 2.70.0 ] - 2017.12.18

* syncro: f__archive_test, f__error, f__info, f__sudo, fscript_cronAnacron, fscript_get_version, fscript_install, fscript_remove, fscript_update

## [ 2.69.0 ] - 2017.12.18

* rewriting: log xorg, pas de recherche dans gdm3 si dm n'est pas gdm
* rewriting: général initialisation variable numérique
* fixed: nombre de touchpad

## [ 2.68.1 ] - 2017.12.17

* rewriting: affichage usb
* fixed: figet_lspci, détection carte video

## [ 2.67.0 ] - 2017.12.16

* rewriting: affichage interface sortie ipv4 & ipv6
* rewriting: f_lifting, motifs suppression
* rewriting: figet_cpu, figet_dmi, fi_gpu, fi_audio, fi_net, motifs suppression
* fixed: figet_cpu, bug ubuntu? non reproductible

## [ 2.66.5 ] - 2017.12.15

* rewriting: fi_usb, présentation pour parsage ultérieur +facile
* révison: affichage fi_reseau
* fixed: mise en page, f_prt

## [ 2.66.2 ] - 2017.12.14

* added: fi_usb, assemblage lsusb & lsusb -t
* added: f_epure_marque, donc révision figet_dmi, figet_lspci
* fixed: détection usb (fi_usb) et wlx (fi_net)
* fixed: fi_touchpad, détection xorg.log inexistant sur wayland et buttons touchpad pas toujours présent dans les logs

## [ 2.65.0 ] - 2017.12.14

* added: fi_touchpad
* rewriting: figet_ip_pub
* rewriting: général, marqueur et appel fonction

## [ 2.64.0 ] - 2017.12.13

* rewriting: fi_bluez, détection device
* rewriting: fi_conf, nouveau fichiers
* rewriting: fi_audio, figet_lspci, figet_modules, détection modules par figet_lspci
* rewriting: figet_lspci, figet_modules, détection commune fichier f_search_ko
* ménage: figet_ucode
* fixed: figet_lspci, détection erronée si interface manquante

## [ 2.63.0 ] - 2017.12.12

* rewriting: général, optimisation affichage et comptage via ls
* rewriting: fi_pkg_apt, fi_pkg_x, figet_batt, figet_disk, figet_distrib, figet_hw, figet_modules
* fixed: fi_system, comptage alim

## [ 2.62.0 ] - 2017.12.11

* rewriting: figet_ip
* rewriting: fi_réseau +ipv6, alert slaac+mac
* rewriting: fi_ssid, détection connexion wifi
* rewriting: f__wget_test

## [ 2.60.0 ] - 2017.12.9

* rewriting: fi_system si plusieurs batteries
* rewriting: figet_batt, protection format numérique
* rewriting: général, force numérique si éventuel pb sur les $g_nb, et test pluriel
* rewriting: figet_de, Kde+version
* fi_pkg_apt: afichage
* fixed: kde, figet_wm, figet_de, 
* fixed: gentoo, test sur répertoire /sys/class/power_supply
* fixed: gentoo, non résolu erreur expression ((+)) sur figet_mem

## [ 2.59.0 ] - 2017.12.8

* rewriting: module, nouvelle méthode pour vidéo et net, figet_lspci, recherche directe module lspci
* rewriting: figet_modules
* rewriting: fi_gpu, fi_net, fi_audio, fi_bluez (modules)
* fixed: fi_system, nombre d'options du boot

## [ 2.58.1 ] - 2017.12.7

* rewriting: f_grep_file, sources affichées en colonnes
* rewriting: fi_pkg_apt, + paquets kernels
* rewriting: fi_efi en catégorie configuration
* fixed: figet_de, gnome-shell

## [ 2.57.1 ] - 2017.12.6

* rewriting: fi_locale, oublié locale
* rewriting: fi_system, uptime avec gawk et selon formats possibles loadAverage, avec calcul fonction threads
* rewriting: fscript_update, controle chargement début et fin
* changed: séquence start pour éviter erreur cron
* rewriting: figet_lspci, \n markdown
* fixed: affichage

## [ 2.56.0 ] - 2017.12.5

* rewriting: fonctions communes
* rewriting: f__requis, f__sudo indépendants de f__which
* rewriting: fscript_cronAnacron, fscript_install, fscript_remove, fscript_update, f__log, renommage  $fileInstall $fileLogs
* rewriting: tags cpu, gawk, présentation
* fixed: f__wget_test
* fixed: fonction manquante
* fixed: fi_mem, fi_nm, fix affichage

## [ 2.54.0 ] - 2017.12.03

* rewriting: f-grep_file, révision, nouvelle option
* rewriting: fi_conf, ajout service systemctl
* rewriting: f_grep_file et nouvelle option comment
* rewriting: fi_pkg_apt, détails conf
* rewriting: fi_log_xorg, mode silent pour confs xorg
* fixed: 2.53.1 push sur un test en cours :(
* fixed: fu_user

## [ 2.53.0 ] - 2017.12.02

* rewriting: fi_dmeg, fi_journal, fu_user, fi_system_analyse, fscript_cronAnacron, fscript_install, fscript_update; renommage variable publique fu_user
* rewriting: fi_gpu, confs particulières Xorg

## [ 2.52.0 ] - 2017.12.02

* added: figet_wm, ajout compositors
* rewriting: fi_gpu, recherche server Xorg
* rewriting: fi_system (serverX)
* rewriting: f__architecture, renommage variable publique

## [ 2.51.2 ] - 2017.12.01

* added: f_pr, f_di nouveau marquage pour construction rapport MD
* rewriting: fi_systeme, version Xserveur X.org, suppression alert_SLiM
* rewriting: alert ucode dans fi_cpu
* rewriting: fi_audio, fi_bluez, fi_disk, fi_vrms
* rewriting: général, bascule sur f_pr, f_di
* rewriting: figet_cpu, nb de cpu
* nettoyage: suppression des tests

## [ 2.50.3 ] - 2017.11.30

* added: figet_modules, maj dynamiques des modules sur le kernel actif
* added: bluetooth
* added: fi_system, panel
* added: fi_net, réseau scindé, partie matérielle, figet_modules
* rewriting: fi_audio, variables publiques, insertion dans fi_system
* rewriting: figet_lspci, renommage, motifs extraction à l'intérieur
* rewriting: fi_audio, fi_graph, fi_net (figet_lspci)
* rewriting: fi_reseau, expurgée du matériel
* rewriting: fi_gpu, renommage fi_graph, utilisation figet_modules
* removed: figet_mod_net, adieu wireless-info
* removed: figet_gpu, suppression, fusion avec fi_gpu
* fixed: fi_bluez, device n/a, alerte

## [ 2.49.3 ] - 2017.11.29

* rewriting: fi-graph, figet_gpu, utilisation f_lspci
* rewriting: figet_ucode, affichage
* rewriting: figet_dmi, fi_lspci, extractions pour affichage
* fixed: fi_audio, modules si multi-devices

## [ 2.48.1 ] - 2017.11.28

* added: fi_audio
* added: f_lspci
* rewriting: fi_reseau, utilisation f_lspci

## [ 2.47.0 ] - 2017.11.27

* added: fi_dmesg, firmware bug, info_ucode
* rewriting: fi_journal, firmware bug autre provenance
* rewriting: général, $IFS_INI
* rewriting: fi_log_xorg
* rewriting: f_grep, +option date, modif fichier
* rewriting: fi_graph, fi_reseau affichage lspci -nnv
* rewriting: fi_disk, df, sélection partitions pour df (issue btrfs openSuse)

## [ 2.46.0 ] - 2017.11.26

* added: fi_conf
* rewriting: f_grep, +option noinexist, +différence fichier vide/inexistant
* rewriting: fi_journal, info_ucode

## [ 2.45.6 ] - 2017.11.25

* added: gestion no $DISPLAY
* rewriting: version bash en test et affichage
* rewriting: figet_shell, fi_systeme, shell & shellS
* rewriting: général, LC_ALL=C oubliés
* rewriting: figet_wm, fi_graph
* rewriting: fi_disk, affichage hddtemp
* rewriting: figet_cpu, affichage unités freq selon le nombre détecté
* fixed: fi_systeme, openSuse uptime
* fixed: figet_de, cinnamon

## [ 2.44.2 ] - 2017.11.24

* added: figet_ucode, indépendant de debian
* rewriting: fi_systeme, microcode
* rewriting: figet_cpu, figet_cpu_uarch, fg_vendor
* rewriting: pas de log xorg si inutile
* rewriting: affichages générals mineurs
* fixed: fi_locale, setxkbmap! merde wayland: setxkbmap+xprop, xrandr, xdpyinfo

## [ 2.43.3 ] - 2017.11.23

* rewriting: général affichage défaut markdown
* rewriting: fi_graph, wayland (final?)
* rewriting: fi_nm, ajout emplacement

## [ 2.43.3 ] - 2017.11.22

* added: fi_serial, machine ID si présent
* rewriting: f_help, nouvelle manière afficher
* fixed: fi_serial, fi_ssid, pas d'utilisation fichier MD
* fixed: f_grep_file, protection espaces dans nom de fichier (NM!..)
* fixed: fi_systeme, erreurs sous fedora
* fixed: f__requis, erreur sur autre que debian

## [ 2.42.3 ] - 2017.11.21

* rewriting: wayland, wayland/root, évolution wayland si plus de xprop, xrandr, xdpyinfo... +70lignes pour ça, environ!
* rewriting: fi_system, figet_de, fi_graph, figet_screen, fi_system_analyse prise en compte wayland
* rewriting: figet_wm, + compositor si connu
* rewriting: fi_log_xorg, + emplacement log sous gdm3 X11 (nouveau sous buster?), cmd et info adaptés
* fixed: fi_system_analyse si graph généré par root

## [ 2.41.3 ] - 2017.11.20

* rewriting: fi_graph, multi-cartes
* rewriting: fi_nm, cas ou pas de wifi à proximité, révision
* rewriting: fi_pkg_apt, protection et alerte si erreur apt-update
* fixed: f_display, fi_reseau, affichage

## [ 2.40.2 ] - 2017.11.19

* added: fi_pkg_apt, alert htps
* rewriting: affichage des commandes dans les conseils
* rewriting: fi_cpu, afichage du nombre de flags 
* fixed: fi_system_analyse, contournement, plante sous buster en root, svg non généré

## [ 2.39.0 ] - 2017.11.19

* added: fi_efi, efi boot
* fixed: f_grep_file, affichage largeur 1ere colonne (sources)

## [ 2.38.9 ] - 2017.11.18

* added: fi_pkg_apt, test clean, autoclean, préférences apt
* added: fi_journal, alerte firmware bug (microcode)
* rewriting: f_dspl_md, bascule sur pager si fichier > 100ko
* rewriting: retour lignes alertes, format markdown
* fixed: fi_pkg_apt, détection full-upgrade
* fixed: fi_disk, détection fichier resume absent 

## [ 2.37.3 ] - 2017.11.17

* rewriting: fi_packager fi_pkg_x
* rewriting: fusion pkg_ctl & fi_packager
* fixed: affichage microcodes installés

## [ 2.36.4 ] - 2017.11.17

* fixed: fi_pkg_apt

## [ 2.36.0 ] - 2017.11.16

* added: fi_pkg_apt, test paquets hold, affichage & non standard
* rewriting: fi_log_xorg, info options appliquées par défaut
* rewriting: fi_serial
* rewriting: fi_system_analyse, fi_systeme, présentation si aucune alerte

## [ 2.35.3 ] - 2017.11.15

* rewriting: f_display, fi_system_analyse, affichage + services critiques, graphique services au chargement
* added: fi_systeme, version systemd
* added: fi_system_analyse, alerte services en erreur
* added: fi_system_analyse, statut des services en erreur
* fixed: fi_vrms
* fixed: figet_dm

## [ 2.34.1 ] - 2017.11.14

* added: deborphan dans fi_pkg_apt
* rewriting: fi_journaux, derniers logs, affichage
* fixed: fi_disk, température via hddtemp
* rewriting: fi_graph

## [ 2.33.0 ] - 2017.11.13

* added: fi_pkg_apt, nouvelle alerte sources: extension non .list 
* rewriting: structure gestionnaires de paquets 

## [ 2.32.0 ] - 2017.11.12

* added: structure pour accueillir autres gestionnaires de paquets que dpkg, nbre de paquets installés si infos trouvées

## [ 2.31.2 ] - 2017.11.12

* fixed: affichage alerte net-tools
* fixed: localisation fichier shells correcte (et non test)

## [ 2.31.0 ] - 2017.11.11

* added: figet_dm, display managers oubliés
* rewriting: fi_systeme, + date installation
* rewriting: shells meilleure extraction pour autres linux, tri, sans doublons
* rewriting: uptime meilleur affichage
* rewriting: fi_vrms
* fixed: détection paquet non installés

## [ 2.30.0 ] - 2017.11.10

* rewriting: fonctions affichage (f_display, f_grep_file, f_dspl_alert, suppression f_dspl_file_KO) 
    * fi_journal, f_disk, fi-graph, fi_locale, fi_reseau, fi_sources, fi_cpu, fi_hw
* fixed: fi_journal, mauvaise détection journaux non persistants

## [ 2.29.0 ] - 2017.11.10

* added: figet_gpu v3, retourne infos et nb de gpu quelque soit options lspci
* rewriting: alerte curl manquant moins déroutant pour nouvel utilisateur
* rewriting: alertes/infos
* rewriting: f_disk
* rewriting: f_graph, glxinfo durci pour configs multi-cartes 
* rewriting: f_grep_file
* fixed: extraction shell

## [ 2.28.0 ] - 2017.11.09

* added: f_dspl_alert, début homogénéisation alertes/infos
* rewriting: figet_mem, protection IFS
* rewriting: fi_journal, meilleur vitesse quand logs nombreux, alerte journaux non persistants
* rewriting: f_dspl_md, espaces en début de ligne préservés, listes

## [ 2.27.0 ] - 2017.11.08

* added: f_dspl_md, moteur d'affiche console (md2console)
* rewriting: affichage f_display
* rewriting: fi_system_analyse, fi_mem: affichage

## [ 2.26.1 ] - 2017.11.07

* rewriting: fi_sources v2
* rewriting: affichage grep files: fi_reseau, fi_locale, fi_log_xorg

## [ 2.25.6 ] - 2017.11.06

* added: n° série comme option, batterie, disk & chassis (si root)
* rewriting: plus d'extraction de n° série en standard (confidentialité) dans batterie & disk
* rewriting: affichage figet_disk, figet_batt
* rewriting: affichage ssid
* rewriting: f__wget_test, maintenance frama.link
* rewriting: fscript_update, contrôle téléchargement durci
* rewriting: fscript_get_version, affichage lors update
* rewriting: fileInstall dans fscript_remove, fscript_install, fscript_cronAnacron
* fixed: ssid, pas de rapport markdown 

## [ 2.24.0 ] - 2017.11.06

* added: type de chassis selon smbios janvier 2017
* rewriting: fi_graph, fi_systeme (alpine linux, busybox) options lspci absentes
* rewriting: figet_batt

## [ 2.23.1 ] - 2017.11.05

* figet_cpu scindé, appel figet_cpu_uarch
* fixed: flags cpu

## [ 2.23.0 ] - 2017.11.04

* added: µarchitecture proc amd
* added: texte des flags cpu
* rewriting: tri des flags cpu
* rewriting: Xorg.log, datage des fichiers

## [ 2.22.1 ] - 2017.11.02

* added: µarchitecture proc intel

## [ 2.21.10 ] - 2017.11.01

* added: µarchitecture proc intel, beta
* fixed: figet_distrib, affichage distrib 

## [ 2.21.7 ] - 2017.10.31

* added: architecture processeur
* cosmetic: tous les variables publiques des fonctions partageables
* rewriting: ajout protection avant suppression newlines
* rewriting: fi_graph
* fixed: alpine linux, FIN!

## [ 2.21.3 ] - 2017.10.30

* added: figet_gpu
* removed: figet_gpu neofetch
* rewriting: fi_graph, lspci, cmd & affichage cmd selon nb gpu
* fixed: affichage/contournement alpine linux
* fixed: fi_vrms

## [ 2.20.3 ] - 2017.10.29

* added: figet_distrib
* added: taille des fichier journaux
* removed: figet_distro neofetch
* fixed: ? bug aléatoire wwn
* fixed: figet_gpu, fix rapide en attendant réécriture

## [ 2.19.6 ] - 2017.10.28

* added: avertissement erreur display :0 en root fedora beta ou buster
* rewriting: message resume
* rewriting: fi_nonFree renommé en fi_vrms + révision pas finie, plus de discrétion si absent
* fixed: fi_nm, permission dans fedora NetworkManager.state
* fixed: figet_dmi, markdown
* changed: figet_batt: s/n sur dell i5

## [ 2.19.1 ] - 2017.10.27

* added: option update spécial, script en place
* rewriting: fscript_update: update spécial
* rewriting: f__log: plus d'avertissement si filelog absent, création
* rewriting: fi_reseau, affichage
* rewriting: figet_ip_pub, renommage fonction & variable public
* rewriting: figet_ip, renommage variable public
* rewriting: fi_reseau, alerte wlx
* removed: fi_nm_wifis, fusion avec fi_nm
* fixed: fi_reseau, détection slot pci, bug sur un pc sour arch
* fixed: figet_batt formatage 0 en décimale si unité batterie en W

## [ 2.18.6 ] - 2017.10.26

* added: figet_screen (nb écrans & résolutions)
* removed: figet_resolution neofetch
* added: xrandr multi-écrans
* rewriting: affichage des fichiers manquants
* rewriting: fi_graph, version 4 (pour fixer sortie sur multi-cartes hybrides) + xrandr current & preferred
* rewriting: fi_reseau, iwlist chan, juste sortie canal utilisé
* fixed: suppression s/n batterie si champs rempli d'espace vide
* fixed: figet_screen, fix nombre d'écrans

## [ 2.17.4 ] - 2017.10.25

* added: nombre d'écrans
* rewriting: batterie, unité en W au lieu de mW, mA inchangé
* fixed: fi_locale compatible fedora (arch ok)
* rewriting: modification complète des séquences peuplement rapport et modules fi_* autonomes
* fixed: bug affichage disque sata
* fixed: f_display, affichage sur --debug-*
* fixed: correction newlines

## [ 2.15.0 ] - 2017.10.24

* added: début support Bumblebee
* rewriting: carteS dans fi_graph
* rewriting: fi_journal, affichage début des logs en titre, suppression dans les logs affichés
* fixed: bug formatage bash quand aucun retour hwmon si bug acpi sondes
* fixed: affichage d'explications état paquet dans fi_sources
* fixed: affichage modules non reconnus fi_reseau

## [ 2.14.2 ] - 2017.10.23

* added: nb alims total/branchée
* rewriting: figet_test_batt (upower)
* fixed: fi_systeme, fi_graph: bug affichage gpu si 2 cartes graphiques
* fixed: figet_batt, return meilleur si pas de batterie

## [ 2.14.0 ] - 2017.10.22

* added: détection connexion ssh
* rewriting: figet_dmi, suppression To be filled by O.E.M.
* rewriting: fi_nm_wifis by-pass wifis à proximité si pas d'interfaces réseau active
* rewriting: figet_hw, filtrage ligne complète si valeur vide, fan ou température
* fixed: ssh, fedora: fi_locale, test si présence xkbmap
* fixed: ssh, fedora: fi_graph, test si présence xrandr
* fixed: ssh, pas de journal xorg si ssh
* fixed: ssh, fi_systeme, figet_resolution, figet_de, figet_wm
* fixed: affichage résolution/opengl
* fixed: figet_batt, return meilleur si pas de batterie
* fixed: fi_hw, by-pass si pas d'info acpi
* fixed: fi_reseau, fi_graph, avertissement modules vide

## [ 2.13.1 ] - 2017.10.22

* added: f__cmd_exist, émule et remplace which debian
* fixed: bug fedora, which supprimé/émulé
* rewriting: affichage avec f_display, fin pour tous les fi_
* cosmetic: pluriel sur titrages markdown

## [ 2.12.1 ] - 2017.10.21

* added: ré-écriture figet_cpu (sans température)
* removed: figet_cpu neofetch
* rewriting: fi_cpu
* rewriting: affichage avec f_display

## [ 2.11.1 ] - 2017.10.20

* rewriting: figet_hw v2, name, label et filtrage 0°C
* rewriting: figet_hw_test, figet_test_batt, figet_test_dmi

## [ 2.10.3 ] - 2017.10.19

* added: fi_journal, journaux kernel et non-kernel via journalctl, dmesg en fallback, x premières lignes
* added: fi_disk, ajout listes partitions fixes/amovibles, montées/non montées, swap
* rewriting: figet_disk, tris des listes de disques par type, partition swap, swap sorti partitions non montées
* rewriting: fi_dmesg, , x premières lignes
* rewriting: suppression option --dmesg, journaux via option -ca (fi_system_analyse fi_log_xorg fi_journal) + option -j
* rewriting: fi_log_xorg, trié par error ou warning, juste .0.log, x premières lignes
* rewriting: fi_nonFree passsé en catégorie configuration (-cc), option équivalente -j
* rewriting: fi_reseau, affichage interface réseau prioritaire SI plusieurs interfaces
* fixed: fi_dmesg, niveau emergency & alerte oublié
* fixed: partitions en liste

## [ 2.9.1 ] - 2017.10.18

* rewriting: réécriture figet_disk, renommage variables publiques
* fixed: dmesg, variable locale

## [ 2.8.8 ] - 2017.10.17

* added: figet_hw_test, acpi -V si disponible, tests (temporaires) sur /sys/class/thermal/ & /sys/devices/virtual/thermal/ & hwmon0(méthode neofetch)
* added: fi_disk, si hddtemp dispo (obsolète?), température et alerte température
* f__requis: pluriel, formatage code
* rewriting: figet_ip_public, fscript_get_version, f__wget_test, fscript_update, suppression option tries personnalisée
* rewriting: fi_disk, comptage avec wc
* rewriting: _test, appel modifié f__scandir pour reporter affichage f__requis à l'écran (et non dans le fichier de sortie)
* rewriting: figet_cpu, suppression affichage température core en attendant mieux
* fixed: sr0 compté en disque hotplug (et non fixe)

## [ 2.7.0 ] - 2017.10.16

* rewriting: f__error f__info f__requis f__wget_test
* rewriting: f__scandir, requis "strings>binutils" dans fonction pour utiliser le script principal en dehors des --debug-*
* fixed: figet_batt, n° de série
* fixed: figet_ip, erreur possible entre lo/certain ifn

## [ 2.6.4 ] - 2017.10.15

* révison: f__scandir

## [ 2.6.3 ] - 2017.10.14

* rewriting: figet_batt: +batt_volt_now, réécriture avec gawk majoritaire, alertes batterie
* rewriting: f__scandir
* rewriting: fipaste_curl_pastery, gestion erreur paste non utf-8
* rewriting: fi_disk, pluriel sur disk fix & hotplug
* fixed: figet_disk, cd/dvd srx pris en compte
* fixed: figet_de correction variable publique
* fixed: figet_hw temp &fan éventuel et test sur présence temp crit et hyst
* fixed: f__scandir bug utf-8, usage strings
* fixed: figet_batt division by 0 (p200)
* fixed: figet_hw plusieurs valeurs(temp) par device (p200)

## [ 2.5.0 ] - 2017.10.13

* réécriture: figet_dmi_test, figet_hw_test (f__scandir)
* added: figet_batt_test

## [ 2.4.2 ] - 2017.10.13

* rewriting: figet_hw_test: +sensors si possible +figet_hw
* changed: figet_hw autres seuils de température 
* fixed: figet_hw formatage float

## [ 2.3.0 ] - 2017.10.12

* added: figet_hw, figet_hw_test
* rewriting: fi_sources: explications état paquet non ii, _alertifié_
* rewriting: suppression fin de ligne $'\n'
* rewriting: figet_battery, figet_mod_net: renommage variables publiques

## [ 2.2.0 ] - 2017.10.11

* added: figet_dmi, figet_dmi_test
* rewriting: présentation pfiget_disk, fi_disk, espace disk dans détails, alert resume
* fixed: figet_battery: erreur anciennes batteries APM ou interface ACPI
* fixed: f__sudo fonctionnement avec sudo

## [ 2.1.0 ] - 2017.10.10

* added: fi_ssid géré via f__sudo (oubli)
* added: fi_systeme charge système
* added: alarm batterie éventuelle
* rewriting: figet_cpu, présentation et $fget_core
* changed: option --dmesg au lieu de -d + log xorg
* rewriting: help
* fixed: fi_batt: pas d'affichage si pas de batterie

## [ 2.0.0 ] - 2017.10.09

* added: 4 catégories de rapport possible, avec menu éventuel pour choix catégorie
* added: f__sudo, dmesg pour user, avec gestion su ou sudo
* added: intégration f__sudo dans install & remove script
* rewriting: option debug
* rewriting: simplification syntaxe options
* rewriting: fileOutput géré par chmod
* changed: log xorg passés en catégorie analyse
* changed: f__user, fonctionnement en root only en console
* rewriting: f__color; utilisation terminfo pour retour au std (et non noir), donc modifs: f__color f__error f__info f__wget_test f__dialog_oui_non fscript_get_version fscript_install, fscript_remove fscript_update
* rewriting: réseau

## [ 1.35.0 ] - 2017.10.05

* added: fi_disk: inoeuds
* rewriting: figet_mem, meilleure précision 

## [ 1.34.1 ] - 2017.10.04

* added:  figet_mem
* removed: figet_memory neofetch
* rewriting: fi_mem, affichage mémoire expurgé comparé à `free`
* rewriting: nommage variables fonctions neofetch
* changed: test une seule fois frama.link

## [ 1.33.0 ] - 2017.10.03

* séparation figet cpu/mem
* fixed: figet_de, fonction neofetch (en aveugle)

## [ 1.32.2 ] - 2017.10.02

* rewriting: sed s/
* rewriting: fi_systeme: boot image avec options
* rewriting: fi_disk
* rewriting: nommage variables fonctions neofetch
* rewriting:variables de boucles en local
* changed: f__user, premier essai root only, fonctionnement en root only en console
* fixed: figet_battery, alim, si AC au lieu de AC0

## [ 1.31.0 ] - 2017.10.01

* rewriting: figet_disk: meilleure détection vendor/model quand vide sur lsblk 
* rewriting: fi_gpu, fi_reseau

## [ 1.30.0 ] - 2017.09.30

* rewriting: fi_systeme, figet_ip, figet_disk, fi_reseau, fi_gpu, figet_battery
* rewriting: fi_sources plus de critère d'architecture dans les paquetsBiz 
* fixed: figet_cpu, fi_sources: comptage ligne avec grep si 0 ligne

## [ 1.29.0 ] - 2017.09.29

* added: figet_battery: voltage, conso si 0
* fixed: f__wget_test, code retour lors "test"
* rewriting: fi_sources, nb de packages installés, date dernier apt update
* figet_cpu, remplacement wc -l par grep -c
* rewriting: plus de dépendances wc 
* fixed: fi_disk, si pas de fichier resume

## [ 1.28.0 ] - 2017.09.28

* added: figet_battery
* rewriting: f__wget_test, nouveau nommage fichier temp
* removed: figet_battery neofetch
* fixed: figet_disk

## [ 1.27.0 ] - 2017.09.27

* changed:: dmesg, test cmd au lieu de test root
* changed: f__user requis uniquement pour install, update, remove script

## [ 1.26.0 ] - 2017.09.26

* added: figet_disk avec nouvelles infos et espace disque des partitions montées seulement
* removed: figet_disk neofetch 
* rewriting: partitions/disques, fi_disk
* changed: test bash4 au démarrage
* changed: requis curl uniquement pour paste

## [ 1.25.0 ] - 2017.09.25

* changed: f__wget_test: nouvelle option test
* liens raccourcis frama.link
* changed: présentation des commandes au format markdown 

## [ 1.24.0 ] - 2017.09.24

* added:  fi_nonFree
* rewriting: fi_dmesg, fi_nm_wifis

## [ 1.23.0 ] - 2017.09.23

* rewriting: unset/for

## [ 1.22.0 ] - 2017.09.22

* added: daemon d'initialisation (systemd ou autre)
* rewriting; système, présentation en table pour meilleure présentation
* rewriting: fi_gpu, réduction verbosité (1v au lieu de 3v)(pour éviter erreur en root avec capabilities) et sélection par slot, présentation
* rewriting: fi_reseau, homogénéité nnv au lieu de nnk et sélection par slot , présentation
* rewriting: fi_sources, présentation et nombres de paquets upgradables
* rewriting: présentaion rapport
* rewriting: présentation fi_dmesg

## [ 1.20.0 ] - 2017.09.18

* rewriting: f__dialog_oui_non
* rewriting: f__affichage (f__color)

## [ 1.19.0 ] - 2017.09.16

* added: timedatectl dans localisation et alertes horloge RTC et Ntp
* rewriting: fi_disk, variable alert_

## [ 1.18.0 ] - 2017.09.15

* fixed: suppression code gui-dialog en dev
* added: ajout date en fin de rapport

## [ 1.17.0 ] - 2017.09.14

* fixed: id resume dans fi_disk

## [ 1.15.0 ] - 2017.09.07

* rewriting: f__wget_test, fscript_get_version, f__log: 
* fixed: f_help

## [ 1.14.0 ] - 2017.09.06

* syncro: fscript_cronAnacron, fscript_update, fscript_install, fscript_remove
* cosmetic: user_

## [ 1.13.0 ] - 2017.09.04

* added: option affichage adresses mac
* added: IFS
* rewriting: figet_ip: 
    * ifnames, toutes les interfaces et pas seulement les connectées
    * mac address des interfaces
* rewriting: affichage fi_ssid
* fixed: export direct après génération rapport

## [ 1.12.0 ] - 2017.09.03

* changed: test connectivité avant recherche ip public pour éviter timeout, figet_ip_public
* rewriting: figet_ip: 2 espaces préliminaires pour meilleure présentation
* rewriting: fscript_remove, fscript_install & fscript_update

## [ 1.10.0 ] - 2017.09.02

* fixed: commande parasite sur détection ip

## [ 1.9.1 ] - 2017.09.01

* rewriting: f__wget_test

## [ 1.9.0 ] - 2017.08.30

* rewriting: f__requis, f__user, f__wget_test, fscript_cronAnacron
* rewriting: déclaration local

## [ 1.8.0 ] - 2017.08.30

* rewriting: fscript_install, fscript_remove(), fscript_update, conditions d'utilisations
* rewriting: fscript_install, fscript_remove(), fscript_update, appel
* cosmetic: f_help 
* changed: options debug et pasteDuration


## [ 1.7.0 ] - 2017.08.28

* rewriting: f__wget_test
* fixed: localisation fileDev

## [ 1.6.0 ] - 2017.08.27

* changed: fscript_cronAnacron, fscript_install : changement lognameDev ->fileDev
* rewriting: fscript_get_version, fscript_install, fscript_remove:  présentation 

## [ 1.5.0 ] - 2017.08.27

* fixed: upgrade

## [ 1.4.3 ] - 2017.08.26

* syncro: fscript_install pour éventuel fscript_install_special
* rewriting: fscript_install, fscript_update, fscript_get_version 
* rewriting: f__wget_test f__error, f__info

## [ 1.3.0 ] - 2017.08.24

* cosmetic: fonctions core

## [ 1.2.1 ] - 2017.08.23

* changed: fscript_cronAnacron: changement délais anacron

## [ 1.2.0 ] - 2017.08.22

* syncro: fscript_dl 
* fixed: f__log manquant

## [ 1.1.0 ] - 2017.08.21

* révison: f__user
* log: pas de maj script
* syncro: fscript_dl, fscript_install, fscript_remove, f__info

## [ 1.0.9 ] - 2017.08.20

* fixed: fscript_cronAnacron, appel fscript_cronAnacron_special
* fixed: $TERM

## [ 1.0.8 ] - 2017.08.19

* rewriting: mise en page
* rewriting: fscript_cronAnacron: maj & plus de redémarrage service cron inutile & fonction spécifique pour certains scripts  service cron restart &>/dev/null || /etc/init.d/cron restart &>/dev/null || f__info "redémarrer cron ou le PC"
* rewriting: fscript_dl plus de sortie progression download
* rewriting: fscript_get_version: inclut version en cours
* rewriting: fscript_install mise en page

## [ 1.0.6 ] - 2017.08.18

* changed: fscript_cronAnacron, maj lors upgrade et spécial pour dev 
* changed: adresse MAC non affichée
* rewriting: ip locales, système et réseau 
* changed: protection absence iproute
* rewriting: test sur which
* added: requis pour fonctionnement script
* syncro: fscript_get_version, fscript_dl, fscript_install: maj 
* fixed: publication version test
* fixed: affichage ipv6, filtre sur inet6 & scope global & adr MAC

## [ 1.0.3 ] - 2017.08.17

* added: get_distro(neofetch)
* rewriting: protection variables
* changed: désactivation affichage ipv6 en attente meilleur filtrage
* fixed: fi_systeme
