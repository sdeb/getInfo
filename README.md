# getInfo

![version: 4.21.1](https://img.shields.io/badge/version-4.21.1-blue.svg?longCache=true&style=for-the-badge)
![bash langage](https://img.shields.io/badge/bash-4-brightgreen.svg?longCache=true&style=for-the-badge)
![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)

> * script bash qui rassemble un certain nombre d'informations sur la configuration d'un PC
> * un rapport au format **markdown** est formé
> * ce rapport peut être exporté sur un pastebin (avec rendu markdown) pour partager les informations
> * par défaut le paste sera conservé 7 jours
> * aucune donnée confidentielle n'est contenue dans le rapport (mot de passe ssid, n° de série, adresses MAC)
> * l'installation du script est possible en option. il se mettra alors éventuellement à jour automatiquement
> * l'option -us permet la mise à jour du script en place, sans installation et sans test ultérieur de 
    nouvelle version 
> * testé sous debian (développement), Ubuntu (notamment LTS 16.04), ArchLinux, openSuse, Fedora et Gentoo
> * tout est collecté avec les droits utilisateur   
   seul le contenu de **journalctl** (ou dmesg) nécessite des droits administrateur et le mot de passe sera demandé.
   À défaut de le connaître ou le saisir, les journaux système ne seront pas affichés.
> * à l'installation, le script crée un lanceur, `gfetch`, qui affiche un résumé système dans le terminal (similaire à neofetch)
> * option possible pour inscrire gfetch dans bashrc (système), résumé du système s'affichera à l'ouverture d'une console 


## chargement et lancement du script:

```shell
wget -nv -O getInfo https://framaclic.org/h/getinfo
chmod +x getInfo
```

* liens de chargement alternatif `wget -nv -O getInfo https://framagit.org/sdeb/getInfo/raw/master/getInfo`

```shell
./getInfo
```
```text
              _   ___        __       
    __ _  ___| |_|_ _|_ __  / _| ___  
   / _' |/ _ \ __|| || '_ \| |_ / _ \ 
  | (_| |  __/ |_ | || | | |  _| (_) |
   \__, |\___|\__|___|_| |_|_|  \___/  
   |___/  version 4.12.0 - 26/06/2018

  •ø••••••••••••••••◇◇◇•••••

  les droits root sont demandés et requis pour afficher les journaux systèmes
  [su] nécessite la saisie du mot de passe root
  [sudo] nécessite le mot de passe utilisateur si celui peut obtenir les privilèges administrateur
  à défaut de saisie valide, les journaux n'apparaîtront pas dans le rapport final

[su]   Mot de passe : 

  le rapport est disponible en local, fichier: getInfo_rapport.md
  vous pouvez le visualiser ultérieurement avec getInfo -l
  vous pourrez aussi l'exporter avec getInfo -p

```

* par défaut, l'export sur un pastebin ne se fera pas (touche <Entrée\>)
* répondre o + <Entrée\> permet d'exporter et de récupérer un lien où le paste sera visible pendant 7 jours en
  standard. Après ce délai il sera supprimé.

```text
  exporter sur le pastebin par défaut? [o/N] o

  
  votre paste:  https://www.pastery.net/abcdef/
  (valide pendant 7 jours)

  le rapport est disponible en local, fichier: getInfo_rapport.md
  vous pouvez le visualiser ultérieurement avec getInfo -l
  vous pourrez l'exporter ultérieurement avec getInfo -p

```

* les erreurs/warnings/critiques via _journalctl_ (ou _dmesg_) ne sont pas rapportées sans saisie de mot de 
  passe superutilisateur. À défaut, les extraits dmesg ne seront pas dans le rapport final.
* export ou non, le rapport au format markdown est disponible dans le répertoire courant, dans le fichier 
  **getInfo_rapport.md**
* ultérieurement, le rapport est consultable avec la commande `./getInfo -l`
* ultérieurement, le rapport peut être (re)exporté avec la commande `./getInfo -p`
* la durée du paste peut être configurée avec l'option supplémentaire `-t n` (n=nombre de jours), par exemple:
    * `./getInfo -p -t 14` pour exporter un rapport existant pour une période de 14 jours
    * `./getInfo -t 21` pour former et exporter un rapport pour une période de 21 jours


## alertes / informations

éventuellement selon les détections, les alertes ou informations suivantes sont affichées:

* analyse boot-systemd
    * des services sont en erreur (alerte) : des services actifs sont dans un état erreur/échec/non trouvé
* disques
    * température de disque > 50°C (information) {hddtemp requis}
    * fichier /etc/initramfs-tools/conf.d/resume absent (information)
    * l'UUID dans resume ne correspond pas à celui du swap (alerte): erreur hibernation et boot allongé
* gestionnaire de paquets Apt/dpkg (debian)
    * apt-transport-https inutile si apt>1.5
    * https avec deb.debian si apt>1.5
    * url httpredir obsolètes dans les sources.list (info)
    * fichiers ayant une extension différente de _.list_ dans /etc/apt/sources.list.d/ (info)
    * erreur apt update (alerte)
    * paquets à mettre à jour `apt upgrade`(info)
    * paquets à mettre à jour avec `apt full-upgrade` (info)
    * paquets inutiles `apt autoremove` (info)
    * archives périmées `apt autoclean` (info)
    * taille du cache des paquets
    * fichiers de configuration de paquets orphelins (^rc) (info)
    * si deborphan, bibliothèques orphelines (info)
* journaux
    * The directory "/usr/share/fonts/X11/cyrillic" does not exist (alert): avertissement comment enlever ce message
    * les journaux ne sont pas persistants (info) : revoir les logs du précédent boot n'est donc pas 
      possible pour investigation (`journalctl --no-hostname --boot -1`) par exemple
    * détection firmware bug (alerte): voir microde? présence ou absence testé dans la partie système
* graphisme
    * une carte graphique semble désactivée (alerte): malgré l'utilisation de **DRI_PRIME=1** ou **optirun**, 
      une carte graphique n'apparaît pas dans lspci
    * l'accélération 3D n'est pas activée (info): voir glxinfo
* locale
    * Network time on: no (info): pas de service NTP actif (synchronisation de l'heure)
    * RTC in local TZ: yes (alerte): L’horloge matérielle est en heure locale et pas en UTC. Les modifications 
      d’heure d’été/hiver peuvent être incohérentes."
* réseau
    * interface wifi en erreur <wlx...> (alerte): l'interface n'est pas reconnue et est donc mal nommée, 
      éventuellement, [changer le renommage](https://kyodev.frama.io/kyopages/trucs/interfaces-nommage-classique/)
    * ifconfig [net-tools](https://github.com/giftnuss/net-tools) est un projet abandonné depuis 
      des années (info): iproute2 [linuxfoundation](https://wiki.linuxfoundation.org/networking/iproute2) le 
      remplace
    * adresses MAC visibles dans les ipv6 SLAAC (info): traçage possible sur les réseaux
* système
    * microcodes intel|amd64 non installés (info): donc pas de corrections bugs du processeur
    * display manager **SLiM** abandonné et non compatible avec systemd (alerte): voir [archlinux](https://wiki.archlinux.org/index.php/SLiM)


## exemple de rapport

[getInfo_rapport](rapport.md)


## help

```shell
./getInfo -h
```
```text
              _   ___        __       
    __ _  ___| |_|_ _|_ __  / _| ___  
   / _' |/ _ \ __|| || '_ \| |_ / _ \ 
  | (_| |  __/ |_ | || | | |  _| (_) |
   \__, |\___|\__|___|_| |_|_|  \___/  -h
   |___/  version 4.12.0 - 26/06/2018
   
  ./getInfo [options]: exécution script (non installé)
    getInfo [options]: exécution script installé dans le système
  -------- Options:
    -c : (catégorie)  menu sélection catégorie d'analyse
        -cs : catégorie système  -cc : catégorie configuration  -csc : système&configuration
        -cr : catégorie réseau   -ca : catégorie analyse        -cscra: toutes les 4 catégories
    -j : (journaux)  analyse démarrage système, log Xorg, kernel et système, catégorie -ca
    -l : (list)  afficher le rapport markdown existant
    -p : (paste) exporte le rapport markdown existant, durée standard du paste 7 jours
      --us     : upgrade spécial du script en place (sans être installé)
      --ip     : affiche ip publique (ipv4/ipv6), pas de rapport markdown
      --mac    : affiche adresses Mac, pas de rapport markdown
      --rc     : affiche un résumé systéme (équivalent gfetch si non installé)
      --serial : affiche n° série disques, batterie et châssis, pas de rapport markdown
      --ssid   : affiche configurations ssid, pas de rapport markdown, root & NetworkManager requis
  
      --debug-paste : affichage retour json de de l'export sur pastebin
      --debug       : messages d'erreur (stderr) logués et exportés avec le rapport
      --dev         : une version de dev du script (si existante) est recherchée
   -t n, --time n   : temps de conservation du paste, par défaut 7 jours
  -------- Script, installation:
    -h, --help    : affichage aide
    -i, --install : installation du script dans le système        (root requis)
    -r, --remove  : désinstallation du script                     (root requis)
    -u, --upgrade : mise à jour script installé
    -v, --version : version du script, installé et en ligne
      --irc       : inscription gfetch dans .bashrc               (root requis) 
      --rrc       : désinscription gfetch dans .bashrc            (root requis)
  -----------------------------------------------------------------------
  plus d'infos: https://framaclic.org/h/doc-getinfo

```

## informations avancées

[getInfo avancé](getInfo_plus.md)


## sources

sur [framagit](https://framagit.org/sdeb/getInfo/blob/master/getInfo)


## contact

pour tout problème ou suggestion concernant ce script, n'hésitez pas à ouvrir une issue 
[Framagit](https://framagit.org/sdeb/getInfo/issues)

IRC: ##sdeb@freenode.net


## license

* pour le code original: [LPRAB/WTFPL](https://framagit.org/sdeb/getInfo/blob/master/LICENSE.md)
* pour les parties de codes incluses, se référer aux licenses spécifiques indiquées dans le fichier   [LICENCE](https://framagit.org/sdeb/getInfo/blob/master/LICENSE.mdd)

## remerciements

* [kernel.org](https://www.kernel.org/) pour les tags cpu, et les modules à détecter
* [neofetch](https://github.com/dylanaraps/neofetch) pour du code sur la détection système.   
  neofetch est un successeur dynamique de screenfetch avec du code sympa 
* [inxi](* https://github.com/smxi/inxi) pour un longue lecture du code
* [pastery.net](https://www.pastery.net/) pour héberger des pastebins avec rendu markdown


![compteur](https://framaclic.org/h/getinfo-gif)
