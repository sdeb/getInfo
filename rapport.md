> getInfo sur **proX2**  

Rapport du 06/11/2017 15:26 +0100 ● /usr/bin/getInfo  ● [getInfo 2.25.6](https://frama.link/doc-getInfo)  


# Système et matériel 


## système  

> **prod.: Hewlett-Packard HP Pro x2 612 G1 Tablet A3009FD10303 (Notebook)   
board: Hewlett-Packard 221B KBC Version 58.50   
bios : Hewlett-PackardM83 Ver. 01.0311/06/2014**  

* CPU 
    * **1 x Intel Core i5-4202Y  (2cores, 4threads) {0.60/1.60/2.00GHz} microcode:0x1c**  
* GPU  
    * **Intel: Device 0a1e**  
* boot  **EFI** 
* distribution **Debian GNU/Linux buster/sid **  

```  
processeur: 64bits  
architecture système: 64bits, amd64  
uname: 4.13.0-1-amd64 x86_64 GNU/Linux  
BOOT_IMAGE=/boot/vmlinuz-4.13.0-1-amd64
   root=UUID=419876c8-c39a-4576-9e55-36fc17d7725c
   ro  
démon d'initialisation: systemd  
serveur d'affichage: wayland  
nombre d'écrans: 1  
résolution: 1920x1080 pixels (59Hz)  
desktop (DE): Gnome  
window manager: GNOME Shell  
shell actif: bash 4.4.12(1)  
shells installés: sh dash bash rbash   
espace des partitions fixes montées (total, utilisé, dispo): 19Go 5,8Go 12Go  
1 alimentation, 0 branchée  
2 batteries présentes:  
  BAT0: Hewlett-Packard (Primary) Li-ion, 3 828mAh - 7.40V / 8.32V (mini/actuel)  
  BAT1: Hewlett-Packard (Secondary) Li-ion, 3 292mAh - 7.50V / 7.79V (mini/actuel)    
dernier boot: 30/10/2017 19:42 +0100, uptime:  15:26:33 depuis 6 jours  
charge système depuis les 1, 5 et 15 dernières minutes: 1.29 0.96 0.52 (4 threads) 
```  


## processeur  

* manuellement voir: `lscpu`   

```  
1 x Intel Core i5-4202Y  (2cores, 4threads) {0.60/1.60/2.00GHz} microcode:0x1c
{fréq. mini/nominale/maxi} GenuineIntel famille 6, modèle 69 {0x06|0x45}, révision 1
39b physique, 48b virtuel, bogomips: 3192, cache: 3072ko  
```  
  
**µarchitecture processeur**  
```  
Intel Haswell 22nm {0x06|0x45}  
```  
  
**flags cpu:**  
```  
90 flags: abm acpi aes aperfmperf apic arat arch_perfmon avx avx2 bmi1 bmi2 bts clflush cmov constant_tsc cpuid cpuid_fault cx16 cx8 de ds_cpl dtes64 dtherm dts epb ept erms est f16c flexpriority fma fpu fsgsbase fxsr ht ida invpcid lahf_lm lm mca mce mmx monitor movbe msr mtrr nonstop_tsc nopl nx pae pat pbe pcid pclmulqdq pdcm pdpe1gb pebs pge pln pni popcnt pse pse36 pts rdrand rdtscp rep_good sdbg sep smep ss sse sse2 sse4_1 sse4_2 ssse3 syscall tm tm2 tpr_shadow tsc tsc_adjust vme vmx vnmi vpid xsave xsaveopt xtopology xtpr  
```  
  
```  
* ABM	⟷ Advanced bit manipulation  
* ACPI	⟷ ACPI via MSR  
* AES	⟷ AES instructions  
* APERFMPERF	⟷ APERFMPERF  
* APIC	⟷ Onboard APIC  
* ARAT	⟷ Always Running APIC Timer  
* ARCH_PERFMON	⟷ Intel Architectural PerfMon  
* AVX	⟷ Advanced Vector Extensions  
* AVX2	⟷ AVX2 instructions  
* BMI1	⟷ 1st group bit manipulation extensions  
* BMI2	⟷ 2nd group bit manipulation extensions  
* BTS	⟷ Branch Trace Store  
* CLFLUSH	⟷ CLFLUSH instruction  
* CMOV	⟷ CMOV instructions (plus FCMOVcc, FCOMI with FPU)  
* CONSTANT_TSC	⟷ TSC ticks at a constant rate  
* CPUID	⟷ CPU has CPUID instruction itself  
* CPUID_FAULT	⟷ Intel CPUID faulting  
* CX16	⟷ CMPXCHG16B  
* CX8	⟷ CMPXCHG8 instruction  
* DE	⟷ Debugging Extensions  
* DS_CPL	⟷ CPL Qual. Debug Store  
* DTES64	⟷ 64-bit Debug Store  
* DTHERM	⟷ Digital Thermal Sensor  
* DTS	⟷ Debug Store  
* EPB	⟷ IA32_ENERGY_PERF_BIAS support  
* EPT	⟷ Intel Extended Page Table  
* ERMS	⟷ Enhanced REP MOVSB/STOSB  
* EST	⟷ Enhanced SpeedStep  
* F16C	⟷ 16-bit fp conversions  
* FLEXPRIORITY	⟷ Intel FlexPriority  
* FMA	⟷ Fused multiply-add  
* FPU	⟷ Onboard FPU  
* FSGSBASE	⟷ {RD/WR}{FS/GS}BASE instructions  
* FXSR	⟷ FXSAVE/FXRSTOR, CR4.OSFXSR  
* HT	⟷ Hyper-Threading  
* IDA	⟷ Intel Dynamic Acceleration  
* INVPCID	⟷ Invalidate Processor Context ID  
* LAHF_LM	⟷ LAHF/SAHF in long mode  
* LM	⟷ Long Mode (x86-64)  
* MCA	⟷ Machine Check Architecture  
* MCE	⟷ Machine Check Exception  
* MMX	⟷ Multimedia Extensions  
* MONITOR	⟷ Monitor/Mwait support  
* MOVBE	⟷ MOVBE instruction  
* MSR	⟷ Model-Specific Registers  
* MTRR	⟷ Memory Type Range Registers  
* NONSTOP_TSC	⟷ TSC does not stop in C states  
* NOPL	⟷ instructions  
* NX	⟷ Execute Disable  
* PAE	⟷ Physical Address Extensions  
* PAT	⟷ Page Attribute Table  
* PBE	⟷ Pending Break Enable  
* PCID	⟷ Process Context Identifiers  
* PCLMULQDQ	⟷ PCLMULQDQ instruction  
* PDCM	⟷ Performance Capabilities  
* PDPE1GB	⟷ GB pages  
* PEBS	⟷ Precise-Event Based Sampling  
* PGE	⟷ Page Global Enable  
* PLN	⟷ Intel Power Limit Notification  
* PNI	⟷ SSE-3  
* POPCNT	⟷ POPCNT instruction  
* PSE	⟷ Page Size Extensions  
* PSE36	⟷ 36-bit PSEs  
* PTS	⟷ Intel Package Thermal Status  
* RDRAND	⟷ The RDRAND instruction  
* RDTSCP	⟷ RDTSCP  
* REP_GOOD	⟷ rep microcode works well  
* SDBG	⟷ Silicon Debug  
* SEP	⟷ SYSENTER/SYSEXIT  
* SMEP	⟷ Supervisor Mode Execution Protection  
* SS	⟷ CPU self snoop  
* SSE	⟷ sse  
* SSE2	⟷ sse2  
* SSE4_1	⟷ SSE-4.1  
* SSE4_2	⟷ SSE-4.2  
* SSSE3	⟷ Supplemental SSE-3  
* SYSCALL	⟷ SYSCALL/SYSRET  
* TM	⟷ Automatic clock control  
* TM2	⟷ Thermal Monitor 2  
* TPR_SHADOW	⟷ Intel TPR Shadow  
* TSC	⟷ Time Stamp Counter  
* TSC_ADJUST	⟷ TSC adjustment MSR 0x3b  
* VME	⟷ Virtual Mode Extensions  
* VMX	⟷ Hardware virtualization  
* VNMI	⟷ Intel Virtual NMI  
* VPID	⟷ Intel Virtual Processor ID  
* XSAVE	⟷ XSAVE/XRSTOR/XSETBV/XGETBV  
* XSAVEOPT	⟷ XSAVEOPT  
* XTOPOLOGY	⟷ cpu topology enum extensions  
* XTPR	⟷ Send Task Priority Messages  
  
```  
  

## mémoire  

```  
mém.:    totale  utilisée  disponible
ram :     3.5Go     1.3Go     1.8Go  

swap:     3.7Go       0ko     3.7Go  

swappiness: 60  
```  
  

## hardware monitor ACPI  

```  
acpitz   43.0/39.0/39.0/31.0°C  (crit: 128.0°C) 
coretemp 45.0/45.0/44.0°C Package.id.0 Core.0 Core.1 (crit: 100.0°C) (maxi: 100.0°C)   
```  
  

## batteries 

```  
BAT0: Hewlett-Packard (Primary) Li-ion, 3 828mAh - 7.40V / 8.32V (mini/actuel)  
pleine charge effective: 3 828mAh, pleine charge théorique: 3 828mAh => 100.0% (indicateur)  
totalement chargée   
BAT1: Hewlett-Packard (Secondary) Li-ion, 3 292mAh - 7.50V / 7.79V (mini/actuel)  
pleine charge effective: 3 292mAh, pleine charge théorique: 3 292mAh => 100.0% (indicateur)  
en décharge, reste approximativement: 1 h 16 mn  (consommation en cours: 2 135mA, charge actuelle: 3 019mAh)    
```  
  

## graphisme  

> * **Intel: Device 0a1e**  

nombre d'écrans: **1**  
résolution: **1920x1080 pixels (59Hz)**  

`lspci -nnv | grep -iEA11 'vga|display|3d'`  
```  
00:02.0 VGA compatible controller [0300]: Intel Corporation Device [8086:0a1e] (rev 0b) (prog-if 00 [VGA controller])
	Subsystem: Hewlett-Packard Company Device [103c:221b]
	Flags: bus master, fast devsel, latency 0, IRQ 51
	Memory at c0000000 (64-bit, non-prefetchable) [size=4M]
	Memory at b0000000 (64-bit, prefetchable) [size=256M]
	I/O ports at 3000 [size=64]
	[virtual] Expansion ROM at 000c0000 [disabled] [size=128K]
	Capabilities: <access denied>
	Kernel driver in use: i915
	Kernel modules: i915  
```  
  
```  
Device: Mesa DRI Intel(R) Haswell (0xa1e)
Direct rendering: Yes  
```  
  
`glxinfo`  
```  
OpenGL vendor: Intel Open Source Technology Center
OpenGL renderer: Mesa DRI Intel(R) Haswell 
OpenGL version: 3.0 Mesa 17.2.4
OpenGL shading language version: 1.30
OpenGL extensions:  
```  
  
`xrandr --verbose | grep -A2 '*current'`  
```  
  1920x1080 (0x22) 173.000MHz -HSync +VSync *current +preferred
        h: width  1920 start 2048 end 2248 total 2576 skew    0 clock  67.16KHz
        v: height 1080 start 1083 end 1088 total 1120           clock  59.96Hz  
```  
  
`xrandr --verbose | grep -A2 '+preferred'`  
```  
  1920x1080 (0x22) 173.000MHz -HSync +VSync *current +preferred
        h: width  1920 start 2048 end 2248 total 2576 skew    0 clock  67.16KHz
        v: height 1080 start 1083 end 1088 total 1120           clock  59.96Hz  
```  
  
`xrandr --listproviders`  
```  
Providers: number : 0  
```  
  
`xrandr --query | grep -A11 'Screen [0-9]'` **(10 premières résolutions possibles)** 
```  
Screen 0: minimum 320 x 200, current 1920 x 1080, maximum 8192 x 8192
XWAYLAND0 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 280mm x 160mm
   1920x1080     59.96*+  
```  
  
### modules video  

**modules recherchés: amdgpu, ati, i915, nouveau, nvidia, radeon, video, gma**  
```  
uvcvideo               90112  0
videobuf2_vmalloc      16384  1 uvcvideo
videobuf2_memops       16384  1 videobuf2_vmalloc
videobuf2_v4l2         24576  1 uvcvideo
videobuf2_core         36864  2 uvcvideo,videobuf2_v4l2
videodev              167936  3 uvcvideo,videobuf2_core,videobuf2_v4l2
media                  40960  2 uvcvideo,videodev
hid_sensor_rotation    16384  0
hid_sensor_trigger     16384  10 hid_sensor_incl_3d,hid_sensor_accel_3d,hid_sensor_magn_3d,hid_sensor_gyro_3d,hid_sensor_rotation
hid_sensor_iio_common    16384  6 hid_sensor_incl_3d,hid_sensor_accel_3d,hid_sensor_trigger,hid_sensor_magn_3d,hid_sensor_gyro_3d,hid_sensor_rotation
industrialio_triggered_buffer    16384  5 hid_sensor_incl_3d,hid_sensor_accel_3d,hid_sensor_magn_3d,hid_sensor_gyro_3d,hid_sensor_rotation
i915                 1552384  31
drm_kms_helper        151552  1 i915
drm                   348160  28 i915,drm_kms_helper
i2c_algo_bit           16384  1 i915
video                  40960  1 i915
button                 16384  1 i915
industrialio           65536  11 hid_sensor_incl_3d,hid_sensor_accel_3d,acpi_als,hid_sensor_trigger,hid_sensor_magn_3d,hid_sensor_gyro_3d,hid_sensor_rotation,industrialio_triggered_buffer,kfifo_buf
hid_sensor_hub         20480  8 hid_sensor_incl_3d,hid_sensor_accel_3d,hid_sensor_iio_common,hid_sensor_trigger,hid_sensor_magn_3d,hid_sensor_gyro_3d,hid_sensor_rotation,hid_sensor_custom
usbcore               245760  8 uvcvideo,usbhid,usb_storage,ehci_hcd,xhci_pci,uas,xhci_hcd,ehci_pci  
```  
  

## disques  

```  
espace des partitions fixes montées (total, utilisé, dispo): 19Go 5,8Go 12Go  

disque fixe      : sda   
disques amovibles: sdb sdc   

partitions fixes montées: sda1 sda2   
partition swap          : sda3   
partition fixe non montée  : sda4   

partitions amovibles montées   : sdb1 sdc1   
partition amovible non montée  : sdc2   

disk  taille   type   vendeur    modèle             rév.
sda   119,2G   Fixe   ATA        MTFDDAV128MAZ-1A   M104  
sdb   14,9G    Amov   Generic-   USB3.0 CRW -SD     1.00  
sdc   3,8G     Amov   USBest Technology USB Mass Storage Device  
```  

**types de disque**  

|    sata   |    usb   |    mmc   |   nvme   |  
|   :---:  |   :---:  |   :---:  |   :---:  |  
|  sda  |  sdb sdc  | -  | -  |  

`df -h --output=source,target,fstype,size,used,avail,pcent --exclude=tmpfs --exclude=devtmpfs` **(utilisation disques)** 
```  
Sys. de fichiers Monté sur                            Type    Taille Utilisé Dispo Uti%
/dev/sda2        /                                    ext4       19G    5,8G   12G  34%
/dev/sda1        /boot/efi                            vfat      120M    121K  120M   1%
/dev/sdb1        /media/kyodev/3131-6663              vfat       15G     32K   15G   1%
/dev/sdc1        /media/kyodev/Debian testing amd64 n iso9660   313M    313M     0 100%  
```  
  
`df -i --exclude=tmpfs --exclude=devtmpfs` **(utilisation inoeuds)** 
```  
Sys. de fichiers  Inœuds IUtil.  ILibre IUti% Monté sur
/dev/sda2        1222992 181710 1041282   15% /
/dev/sda1              0      0       0     - /boot/efi
/dev/sdb1              0      0       0     - /media/kyodev/3131-6663
/dev/sdc1              0      0       0     - /media/kyodev/Debian testing amd64 n  
```  
  
`lsblk -o NAME,FSTYPE,SIZE,LABEL,MOUNTPOINT,UUID` **(disques)** 
```  
NAME   FSTYPE    SIZE LABEL                  MOUNTPOINT                           UUID
sda            119,2G                                                             
├─sda1 vfat      121M                        /boot/efi                            F973-D6D3
├─sda2 ext4     18,6G system                 /                                    419876c8-c39a-4576-9e55-36fc17d7725c
├─sda3 swap      3,7G                        [SWAP]                               198ce357-91ee-492a-9c5c-0fd40bbc0b94
└─sda4            75G                                                             
sdb             14,9G                                                             
└─sdb1 vfat     14,9G                        /media/kyodev/3131-6663              3131-6663
sdc    iso9660   3,8G Debian testing amd64 n                                      2017-09-18-04-24-02-00
├─sdc1 iso9660   313M Debian testing amd64 n /media/kyodev/Debian testing amd64 n 2017-09-18-04-24-02-00
└─sdc2 vfat      416K Debian testing amd64 n                                      9C20-FC5F  
```  
  
`grep -Ev '^[[:blank:]]*#|^[[:space:]]*$' /etc/fstab` **(fstab)** 
```  
UUID=419876c8-c39a-4576-9e55-36fc17d7725c /               ext4    errors=remount-ro 0       1
UUID=F973-D6D3  /boot/efi       vfat    umask=0077      0       1
UUID=198ce357-91ee-492a-9c5c-0fd40bbc0b94 none            swap    sw              0       0  
```  
  
`grep -Evs '^[[:blank:]]*#|^[[:space:]]*$' /etc/initramfs-tools/conf.d/resume` **(resume)** 
```  
RESUME=UUID=198ce357-91ee-492a-9c5c-0fd40bbc0b94  
```  
  

## USB  

`lsusb`  
```  
Bus 001 Device 002: ID 8087:8000 Intel Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 006: ID 0bda:0307 Realtek Semiconductor Corp. 
Bus 003 Device 002: ID 0424:5534 Standard Microsystems Corp. Hub
Bus 003 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 002 Device 006: ID 04f2:b43e Chicony Electronics Co., Ltd 
Bus 002 Device 004: ID 04f2:b43f Chicony Electronics Co., Ltd 
Bus 002 Device 015: ID 1307:0165 Transcend Information, Inc. 2GB/4GB/8GB Flash Drive
Bus 002 Device 014: ID 0e8f:00a5 GreenAsia Inc. 
Bus 002 Device 003: ID 0424:2134 Standard Microsystems Corp. Hub
Bus 002 Device 002: ID 03eb:8a3a Atmel Corp. 
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub  
```  
  
`lsusb -t`  
```  
/:  Bus 03.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/4p, 5000M
    |__ Port 2: Dev 2, If 0, Class=Hub, Driver=hub/4p, 5000M
        |__ Port 4: Dev 6, If 0, Class=Mass Storage, Driver=usb-storage, 5000M
/:  Bus 02.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/9p, 480M
    |__ Port 1: Dev 2, If 0, Class=Human Interface Device, Driver=usbhid, 12M
    |__ Port 1: Dev 2, If 1, Class=Human Interface Device, Driver=usbhid, 12M
    |__ Port 2: Dev 3, If 0, Class=Hub, Driver=hub/4p, 480M
        |__ Port 1: Dev 14, If 0, Class=Human Interface Device, Driver=usbhid, 1.5M
        |__ Port 1: Dev 14, If 1, Class=Human Interface Device, Driver=usbhid, 1.5M
        |__ Port 2: Dev 15, If 0, Class=Mass Storage, Driver=usb-storage, 480M
    |__ Port 3: Dev 4, If 0, Class=Video, Driver=uvcvideo, 480M
    |__ Port 3: Dev 4, If 1, Class=Video, Driver=uvcvideo, 480M
    |__ Port 7: Dev 6, If 1, Class=Video, Driver=uvcvideo, 480M
    |__ Port 7: Dev 6, If 0, Class=Video, Driver=uvcvideo, 480M
/:  Bus 01.Port 1: Dev 1, Class=root_hub, Driver=ehci-pci/3p, 480M
    |__ Port 1: Dev 2, If 0, Class=Hub, Driver=hub/8p, 480M  
```  
  

# Configuration 


## localisation  

`grep -Esv '#|^[[:space:]]*$' /etc/default/locale* /etc/locale.conf`  
```  
/etc/default/locale:LANG="fr_FR.UTF-8"  
```  
  
`localectl --no-pager status`  
```  
   System Locale: LANG=fr_FR.UTF-8
       VC Keymap: n/a
      X11 Layout: fr
       X11 Model: pc105
     X11 Variant: latin9  
```  
  
`grep -Hs . /etc/timezone*`  
```  
/etc/timezone:Europe/Paris  
```  
  
`timedatectl  status --no-pager`  
```  
                      Local time: lun. 2017-11-06 15:26:36 CET
                  Universal time: lun. 2017-11-06 14:26:36 UTC
                        RTC time: lun. 2017-11-06 14:26:36
                       Time zone: Europe/Paris (CET, +0100)
       System clock synchronized: yes
systemd-timesyncd.service active: yes
                 RTC in local TZ: no  
```  
  
`grep -EHv '#|^[[:space:]]*$' /etc/default/keyboard*`  
```  
/etc/default/keyboard:XKBMODEL="pc105"
/etc/default/keyboard:XKBLAYOUT="fr"
/etc/default/keyboard:XKBVARIANT="latin9"
/etc/default/keyboard:XKBOPTIONS=""
/etc/default/keyboard:BACKSPACE="guess"  
```  
  
`setxkbmap -query`  
```  
rules:      evdev
model:      pc105
layout:     us  
```  
  

## paquets non-libres 

`vrms`  
```  
               Non-free packages installed on proX2

broadcom-sta-dkms                   dkms source for the Broadcom STA Wireless driver

  1 non-free packages, 0.1% of 1889 installed packages.  
```  
  

## sources liste  

`grep -Ersv '^#|^[[:space:]]*$' /etc/apt/sources.list /etc/apt/sources.list.d/*.list`  
```  
/etc/apt/sources.list:  deb  https://deb.debian.org/debian/  testing  main  contrib  non-free
/etc/apt/sources.list:  deb  https://deb.debian.org/debian-security  testing/updates  main  contrib  non-free  
```  
  
nombre de paquets installés: **1889** 

dernière mise à jour apt: **06/11/2017 15:26 +0100** 

**21 paquets installés inutiles, vous pouvez utiliser: `apt autoremove`**  
```  
gir1.2-nm-1.0  libfwupd1  libpython3.5  python3.5  libpython3.5-stdlib  python3.5-minimal  libpython3.5-minimal    
```  
  
**15 paquets à mettre à jour, `apt list --upgradable`:**  
```  
gjs/testing 1.50.1-2 amd64 [upgradable from: 1.46.0-1+b2]
gnome-characters/testing 3.26.2-1 amd64 [upgradable from: 3.26.1-2]
gnome-control-center/testing 1:3.26.1-2 amd64 [upgradable from: 1:3.24.3-1+b1]
gnome-control-center-data/testing 1:3.26.1-2 all [upgradable from: 1:3.24.3-1]
gnome-documents/testing 3.26.1-2 amd64 [upgradable from: 3.22.5-2]
gnome-settings-daemon/testing 3.26.2-1 amd64 [upgradable from: 3.24.3-1]
gnome-shell/testing 3.26.1-3 amd64 [upgradable from: 3.22.3-3]
gnome-shell-common/testing 3.26.1-3 all [upgradable from: 3.22.3-3]
gnome-shell-extensions/testing 3.26.1-2 all [upgradable from: 3.22.2-2]
gnome-sushi/testing 3.24.0-1+b1 amd64 [upgradable from: 3.24.0-1]
gnome-tweak-tool/testing 3.26.2.1-2 all [upgradable from: 3.22.0-1]
gnome-weather/testing 3.26.0-2 all [upgradable from: 3.24.0-1]
mutter/testing 3.26.1-6 amd64 [upgradable from: 3.22.4-2]
mutter-common/testing 3.26.1-6 all [upgradable from: 3.22.4-2]
polari/testing 3.26.1-2 amd64 [upgradable from: 3.24.2-1]  
```  
  

# Réseau 


## réseau  

`lspci -nnv | grep -EiA 15 'network|ethernet`  
```  
00:19.0 Ethernet controller [0200]: Intel Corporation Ethernet Connection I218-LM [8086:155a] (rev 04)
	Subsystem: Hewlett-Packard Company Ethernet Connection I218-LM [103c:221b]
	Flags: bus master, fast devsel, latency 0, IRQ 52
	Memory at c0700000 (32-bit, non-prefetchable) [size=128K]
	Memory at c073d000 (32-bit, non-prefetchable) [size=4K]
	I/O ports at 3080 [disabled] [size=32]
	Capabilities: <access denied>
	Kernel driver in use: e1000e
	Kernel modules: e1000e
02:00.0 Network controller [0280]: Broadcom Limited BCM43142 802.11b/g/n [14e4:4365] (rev 01)
	Subsystem: Hewlett-Packard Company BCM43142 802.11b/g/n [103c:2232]
	Flags: bus master, fast devsel, latency 0, IRQ 19
	Memory at c0500000 (64-bit, non-prefetchable) [size=32K]
	Capabilities: <access denied>
	Kernel driver in use: wl
	Kernel modules: wl  
```  
  
**IP locale(s):**  
```  
  192.168.1.114/24 ( enp0s25 ) (ethernet)
  192.168.1.113/24 ( wlo1 ) (wifi)  
```  
  
* les adresses Mac peut être affichées avec `./getInfo --mac` ou `getInfo --mac` (script installé)  
* l'IP publique peut être connue avec: `./getInfo --ip` ou `getInfo --ip` (script installé)  

**Passerelle(s):**  
```  
  192.168.1.5 ( enp0s25 )
  192.168.1.5 ( wlo1 )  fe80::160c:76ff:fe54:7490 ( enp0s25 )
  fe80::160c:76ff:fe54:7490 ( wlo1 )  
```  
  
**interface prioritaire**  
```  
192.168.1.114  
```  
  
`ip address` **(sans ipV6 et sans adresses MAC)** 
```  
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
2: enp0s25: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    inet 192.168.1.114/24 brd 192.168.1.255 scope global dynamic enp0s25
3: wlo1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    inet 192.168.1.113/24 brd 192.168.1.255 scope global dynamic wlo1  
```  
  
`ip route show`  
```  
default via 192.168.1.5 dev enp0s25 proto static metric 100 
default via 192.168.1.5 dev wlo1 proto static metric 600 
169.254.0.0/16 dev wlo1 scope link metric 1000 
192.168.1.0/24 dev enp0s25 proto kernel scope link src 192.168.1.114 metric 100 
192.168.1.0/24 dev wlo1 proto kernel scope link src 192.168.1.113 metric 600   
```  
  
`grep -EHrsv '#|^[[:space:]]*$'  /etc/network/interfaces*`  
```  
/etc/network/interfaces: source /etc/network/interfaces.d/*
/etc/network/interfaces: auto lo
/etc/network/interfaces: iface lo inet loopback  
```  
  
`cat /etc/resolv.conf` **(serveurs de noms DNS utilisés)** 
```  
nameserver 37.187.16.17
nameserver 52.174.55.168
nameserver 80.67.169.12
nameserver 80.67.169.40
nameserver 2a01:e00::1
nameserver 2a01:e00::2  
```  
  
`iwconfig` **état carte wifi** 
```  
wlo1      IEEE 802.11  ESSID:"freemfi"  
          Mode:Managed  Frequency:2.452 GHz  Access Point: 14:0C:76:54:74:91   
          Bit Rate=72 Mb/s   Tx-Power=200 dBm   
          Retry short limit:7   RTS thr:off   Fragment thr:off
          Power Management:off
          Link Quality=70/70  Signal level=-26 dBm  
          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
          Tx excessive retries:0  Invalid misc:0   Missed beacon:0  
```  
  
`iwlist chan | grep 'Current Frequency' | grep -Eio 'channel [0-9]+'` **canal wifi utilisé** 
```  
Channel 9  
```  
  
* la configuration ssid utilisée peut être connue (si NetworkManager utilisé) avec 
`./getInfo --ssid` ou `getInfo --ssid` (script installé)  

### gestionnaire de réseau  

installé: **NetworkManager**  
en fonctionnement: **/usr/sbin/NetworkManager --no-daemon**  

### modules réseau  

**liste non garantie complète**  
```  
wmi_bmof               16384  0
hp_wmi                 16384  0
wl                   6447104  0
cfg80211              602112  1 wl
rfkill                 24576  5 hp_wmi,cfg80211
wmi                    24576  2 wmi_bmof,hp_wmi
sparse_keymap          16384  2 intel_vbtn,hp_wmi  
```  
  

## NetworkManager 

`grep -Ev '#|^[[:space:]]*$' /var/lib/NetworkManager/NetworkManager.state`  
```  
[main]
NetworkingEnabled=true
WirelessEnabled=true
WWANEnabled=true  
```  
  
`grep -Ev '#|^[[:space:]]*$' /etc/NetworkManager/NetworkManager.conf`  
```  
[main]
plugins=ifupdown,keyfile
[ifupdown]
managed=false  
```  
  
## wifis à proximité 

`nmcli -f SSID,BSSID,MODE,CHAN,FREQ,RATE,SIGNAL,BARS,SECURITY device wifi list | head -n15`  
```  
SSID             BSSID              MODE   CHAN  FRÉQ      DÉBIT    SIGNAL  BARS  SÉCURITÉ    
FreeWifi         14:0C:76:54:74:92  Infra  9     2452 MHz  54 Mo/s  100     ▂▄▆█  --          
FreeWifi_secure  14:0C:76:54:74:93  Infra  9     2452 MHz  54 Mo/s  100     ▂▄▆█  WPA2 802.1X 
freemfi          14:0C:76:54:74:91  Infra  9     2452 MHz  54 Mo/s  95      ▂▄▆█  WPA2        
FreeWifi_secure  F4:CA:E5:D2:6E:76  Infra  11    2462 MHz  54 Mo/s  42      ▂▄__  WPA2 802.1X 
FreeWifi         F4:CA:E5:D2:6E:75  Infra  11    2462 MHz  54 Mo/s  42      ▂▄__  --          
KLAC2Free        F4:CA:E5:D2:6E:74  Infra  11    2462 MHz  54 Mo/s  42      ▂▄__  WPA2        
Freebox-78167B   00:24:D4:DF:07:E0  Infra  1     2412 MHz  54 Mo/s  35      ▂▄__  WPA2          
```  
  

# Analyse 


## analyse boot 

**durée de boot:** 12.429s (firmware) + 6.559s (loader) + 4.975s (kernel) + 8.045s (espace utilisateur) = **32.010s** 

`systemd-analyze blame | head -n 20`  
```  
         12.001s apt-daily.service
          5.859s NetworkManager-wait-online.service
          3.030s iio-sensor-proxy.service
          1.548s keyboard-setup.service
           413ms dev-sda2.device
           173ms systemd-localed.service
           169ms ModemManager.service
           150ms upower.service
           146ms packagekit.service
           136ms systemd-timesyncd.service
            89ms colord.service
            81ms speech-dispatcher.service
            81ms systemd-journald.service
            78ms systemd-logind.service
            77ms systemd-udev-trigger.service
            77ms pppd-dns.service
            75ms rsyslog.service
            75ms gdm.service
            71ms systemd-rfkill.service
            67ms systemd-timedated.service  
```  
  

## journaux Xorg 

`grep -Es '\(WW\)|\(EE\)|\(\?\?\)' /var/log/Xorg.?.log /home/<user>/.local/share/xorg/Xorg.?.log` **(Xorg.log)** 
```  
/var/log/Xorg.0.log : <inexistant>  

/home/kyodev/.local/share/xorg/Xorg.0.log, date de modification: 20/10/2017 01:21 +0200 
    (WW) **warning**, (EE) **erreur**, (??) inconnu, 50 premières lignes 
[    21.711] (EE) systemd-logind: failed to take device /dev/ttyS0: No such device
[    21.712] (EE) xf86OpenSerial: Cannot open device /dev/ttyS0
[    21.712] (EE) Serial Wacom Tablet WACf401 PNP0501: Error opening /dev/ttyS0 (Permission denied)
[    21.712] (EE) PreInit returned 8 for "Serial Wacom Tablet WACf401 PNP0501"
[ 36446.676] (EE) libinput bug: timer: offset negative (-1097053)
[ 36446.676] (EE) libinput bug: timer: offset negative (-498467)
[ 36446.676] (EE) libinput bug: timer: offset negative (-174749)
[ 36446.740] (EE) systemd-logind: failed to release device: Device not taken
[ 36446.766] (EE) systemd-logind: failed to release device: Device not taken 
[    21.257] (WW) The directory "/usr/share/fonts/X11/cyrillic" does not exist.
[    21.272] (WW) Falling back to old probe method for fbdev
[    21.272] (WW) Falling back to old probe method for vesa
[    21.272] (WW) VGA arbiter: cannot open kernel arbiter, no multi-card support
[ 36497.171] (WW) Option "xkb_options" requires a string value
[ 36497.194] (WW) Option "xkb_options" requires a string value
[ 38009.678] (WW) xf86CloseConsole: KDSETMODE failed: Input/output error
[ 38009.678] (WW) xf86CloseConsole: VT_GETMODE failed: Input/output error
[ 38009.678] (WW) xf86CloseConsole: VT_ACTIVATE failed: Input/output error    
```  
  

## journalctl kernel (emergency, alert, erreur, warning ou critique) 

**Logs begin at Mon 2017-10-30 19:42:00 CET** 

`journalctl --no-hostname --boot 0 -k -p 1` **(kernel emergency 0 & alerte 1, 25 premières lignes)** 
```  
  <vide>  
```  
  
`journalctl --no-hostname --boot 0 -k -p 2..2` **(kernel critique, 25 premières lignes)** 
```  
  <vide>  
```  
  
`journalctl --no-hostname --boot 0 -k -p 3..3` **(kernel erreur, 25 premières lignes)** 
```  
Oct 30 19:42:00 kernel: [Firmware Bug]: TSC_DEADLINE disabled due to Errata; please update microcode to version: 0x20 (or later)
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to set a report to device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to retrieve report from device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to retrieve report from device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to set a report to device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to retrieve report from device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to retrieve report from device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to set a report to device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to set a report to device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to set a report to device.
Oct 30 19:42:02 kernel: i2c_hid i2c-SMO91D0:00: failed to retrieve report from device.
Oct 30 19:42:04 kernel: ERROR @wl_notify_scan_status : 
Oct 30 23:31:05 kernel: [drm:intel_set_cpu_fifo_underrun_reporting [i915]] *ERROR* uncleared fifo underrun on pipe A
Oct 30 23:31:05 kernel: [drm:intel_cpu_fifo_underrun_irq_handler [i915]] *ERROR* CPU pipe A FIFO underrun
Nov 01 14:33:25 kernel: ERROR @wl_notify_scan_status : 
Nov 01 14:33:25 kernel: ACPI Exception: AE_BAD_PARAMETER, Returned by Handler for [EmbeddedControl] (20170531/evregion-300)
Nov 01 14:33:25 kernel: ACPI Error: 
Nov 01 14:33:25 kernel: pciehp 0000:00:1c.4:pcie004: Device 0000:03:00.0 already exists at 0000:03:00, cannot hot-add
Nov 01 14:33:25 kernel: pciehp 0000:00:1c.4:pcie004: Cannot add device at 0000:03:00
Nov 01 14:33:25 kernel: usb 3-2.4: usb_reset_and_verify_device Failed to disable LTM
Nov 01 14:33:26 kernel: ERROR @wl_notify_scan_status : 
Nov 01 18:02:32 kernel: pciehp 0000:00:1c.4:pcie004: Device 0000:03:00.0 already exists at 0000:03:00, cannot hot-add
Nov 01 18:02:32 kernel: pciehp 0000:00:1c.4:pcie004: Cannot add device at 0000:03:00
Nov 01 18:02:32 kernel: usb 3-2.4: usb_reset_and_verify_device Failed to disable LTM
Nov 01 18:02:33 kernel: ERROR @wl_notify_scan_status :   
```  
  
`journalctl --no-hostname --boot 0 -k -p 4..4` **(kernel warning, 25 premières lignes)** 
```  
Oct 30 19:42:00 kernel: ENERGY_PERF_BIAS: Set to 'normal', was 'performance'
Oct 30 19:42:00 kernel: ENERGY_PERF_BIAS: View and update with x86_energy_perf_policy(8)
Oct 30 19:42:00 kernel:  #2 #3
Oct 30 19:42:00 kernel: PCCT header not found.
Oct 30 19:42:00 kernel: (NULL device *): hwmon_device_register() is deprecated. Please convert the driver to use hwmon_device_register_with_info().
Oct 30 19:42:00 kernel: usb: port power management may be unreliable
Oct 30 19:42:00 kernel: psmouse serio3: synaptics: The touchpad can support a better bus than the too old PS/2 protocol. Make sure MOUSE_PS2_SYNAPTICS_SMBUS and RMI4_SMB are enabled to get a better touchpad experience.
Oct 30 19:42:01 kernel: wl: loading out-of-tree module taints kernel.
Oct 30 19:42:01 kernel: wl: module license 'MIXED/Proprietary' taints kernel.
Oct 30 19:42:01 kernel: Disabling lock debugging due to kernel taint
Oct 30 19:42:01 kernel: wlan0: Broadcom BCM4365 802.11 Hybrid Wireless Controller 6.30.223.271 (r587334)
Oct 30 19:42:01 kernel: 
Oct 30 19:42:02 kernel: uvcvideo 2-3:1.0: Entity type for entity Realtek Extended Controls Unit was not initialized!
Oct 30 19:42:02 kernel: uvcvideo 2-3:1.0: Entity type for entity Extension 4 was not initialized!
Oct 30 19:42:02 kernel: uvcvideo 2-3:1.0: Entity type for entity Processing 2 was not initialized!
Oct 30 19:42:02 kernel: uvcvideo 2-3:1.0: Entity type for entity Camera 1 was not initialized!
Oct 30 19:42:02 kernel: uvcvideo 2-7:1.0: Entity type for entity Extension 4 was not initialized!
Oct 30 19:42:02 kernel: uvcvideo 2-7:1.0: Entity type for entity Extension 2 was not initialized!
Oct 30 19:42:02 kernel: uvcvideo 2-7:1.0: Entity type for entity Processing 3 was not initialized!
Oct 30 19:42:02 kernel: uvcvideo 2-7:1.0: Entity type for entity Camera 1 was not initialized!
Oct 30 19:42:04 kernel: wlo1 Scan_results error (-22)
Oct 30 19:42:28 kernel: FAT-fs (sdb1): Volume was not properly unmounted. Some data may be corrupt. Please run fsck.
Nov 01 14:33:25 kernel: Suspending console(s) (use no_console_suspend to debug)
Nov 01 14:33:25 kernel: wlo1 Scan_results error (-22)
Nov 01 14:33:25 kernel: Method parse/execution failed \_SB.PCI0.LPCB.EC0._Q70, AE_BAD_PARAMETER (20170531/psparse-550)  
```  
  
## journalctl hors kernel (emergency, alert, erreur, warning ou critique) 

**Logs begin at Mon 2017-10-30 19:42:00 CET** 

`journalctl --no-hostname --boot 0 -p 1 | grep -v kernel` **(hors kernel, emergency 0 & alerte 1, 25 premières lignes)** 
```  
  <vide>  
```  
  
`journalctl --no-hostname --boot 0 -p 2..2 | grep -v kernel` **(hors kernel, critique, 25 premières lignes)** 
```  
  <vide>  
```  
  
`journalctl --no-hostname --boot 0 -p 3..3 | grep -v kernel` **(hors kernel, erreur, 25 premières lignes)** 
```  
Oct 30 19:42:02 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-logind.service failed: Invalid argument
Oct 30 19:42:02 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-hostnamed.service failed: Invalid argument
Oct 30 19:42:05 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-localed.service failed: Invalid argument
Oct 30 19:42:29 pulseaudio[977]: [pulseaudio] bluez5-util.c: GetManagedObjects() failed: org.freedesktop.DBus.Error.TimedOut: Failed to activate service 'org.bluez': timed out (service_start_timeout=25000ms)
Oct 30 20:10:02 anacron[1531]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Oct 30 20:10:07 anacron[1534]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Oct 30 20:10:12 anacron[1529]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Oct 30 20:10:15 anacron[1527]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
                        .
Nov 01 14:33:26 systemd-udevd[4148]: inotify_add_watch(9, /dev/sdb, 10) failed: No such file or directory
Nov 01 14:33:30 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-hostnamed.service failed: Invalid argument
Nov 01 14:48:25 anacron[4151]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
                        .
Nov 01 18:02:33 systemd-udevd[6713]: inotify_add_watch(9, /dev/sdb, 10) failed: No such file or directory
Nov 01 18:03:07 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-hostnamed.service failed: Invalid argument
Nov 02 05:22:56 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-hostnamed.service failed: Invalid argument
                        .
Nov 06 09:31:55 systemd-udevd[17176]: inotify_add_watch(9, /dev/sdb, 10) failed: No such file or directory
Nov 06 09:32:08 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-hostnamed.service failed: Invalid argument
Nov 06 10:10:05 anacron[20478]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Nov 06 10:10:10 anacron[20482]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Nov 06 10:10:10 anacron[20479]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Nov 06 10:10:13 anacron[20473]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Nov 06 10:10:16 anacron[20480]: Can't find sendmail at /usr/sbin/sendmail, not mailing output
Nov 06 11:49:06 systemd[1]: Attaching egress BPF program to cgroup /sys/fs/cgroup/unified/system.slice/systemd-hostnamed.service failed: Invalid argument  
```  
  
`journalctl --no-hostname --boot 0 -p 4..4 | grep -v kernel` **(hors kernel, warning, 25 premières lignes)** 
```  
Oct 30 19:42:02 udisksd[528]: failed to load module mdraid: libbd_mdraid.so.2: cannot open shared object file: No such file or directory
Oct 30 19:42:02 udisksd[528]: Failed to load a libblockdev plugin
Oct 30 19:42:02 NetworkManager[552]: <warn>  [1509388922.7517] interfaces file /etc/network/interfaces.d/* doesn't exist
Oct 30 19:42:04 org.gnome.Shell.desktop[619]: glamor: EGL version 1.4 (DRI2):
Oct 30 19:42:05 systemd[1]: wacom-inputattach@ttyS0.service: Failed with result 'exit-code'.
Oct 30 19:42:05 iio-sensor-prox[529]: Failed to read float from /sys/devices/pci0000:00/INT33C2:00/i2c-0/i2c-SMO91D0:00/0018:0483:91D1.0001/HID-SENSOR-200073.2.auto/iio:device1/in_scale
Oct 30 19:42:05 iio-sensor-prox[529]: Failed to read float from /sys/devices/pci0000:00/INT33C2:00/i2c-0/i2c-SMO91D0:00/0018:0483:91D1.0001/HID-SENSOR-200073.2.auto/iio:device1/in_offset
Oct 30 19:42:06 xbrlapi.desktop[850]: openConnection: connect: Aucun fichier ou dossier de ce type
Oct 30 19:42:06 xbrlapi.desktop[850]: cannot connect to braille devices daemon brltty at :0
Oct 30 19:42:07 gsd-power[775]: gsd_power_backlight_abs_to_percentage: assertion 'max > min' failed
Oct 30 19:42:24 org.gnome.Shell.desktop[935]: glamor: EGL version 1.4 (DRI2):
Oct 30 19:42:26 pkexec[1173]: Debian-gdm: Error executing command as another user: Not authorized [USER=root] [TTY=unknown] [CWD=/var/lib/gdm3] [COMMAND=/usr/lib/gnome-settings-daemon/gsd-backlight-helper --set-brightness 918]
Oct 30 19:42:26 org.gnome.SettingsDaemon.Power.desktop[775]: Error executing command as another user: Not authorized
Oct 30 19:42:26 org.gnome.SettingsDaemon.Power.desktop[775]: This incident has been reported.
Oct 30 19:42:28 gnome-software[1194]: plugin shell-extensions took 0,7 seconds to do setup
Oct 30 19:42:29 tracker-miner-f[1186]: Could not set mount point in database 'urn:nepomuk:datasource:3131-6663', GDBus.Error:org.freedesktop.Tracker1.SparqlError.Internal: UNIQUE constraint failed: nie:DataObject.nie:url
Oct 30 19:42:30 gnome-software[1194]: plugin appstream took 1,1 seconds to do setup
Oct 30 19:42:30 gnome-software[1194]: failed to call gs_plugin_add_updates_historical on fwupd: The name org.freedesktop.fwupd was not provided by any .service files
Oct 30 19:43:43 hexchat[1299]: invalid source position for vertical gradient
Oct 30 19:43:43 hexchat[1299]: invalid source position for vertical gradient
Oct 30 19:43:43 hexchat[1299]: invalid source position for vertical gradient
Oct 30 19:43:43 hexchat[1299]: invalid source position for vertical gradient
Oct 30 19:43:45 hexchat[1299]: invalid source position for vertical gradient
Oct 30 19:43:45 hexchat[1299]: invalid source position for vertical gradient
Oct 30 19:44:00 hexchat[1299]: invalid source position for vertical gradient  
```  
  
**les 25 premières lignes commencent à la date du dernier boot**  

`journalctl --disk-usage ` **taille des journaux** 
```  
13.5 Mo  
```  
  

---  

Rapport du 06/11/2017 15:26 +0100 ● /usr/bin/getInfo  ● [getInfo 2.25.6](https://frama.link/doc-getInfo)  

