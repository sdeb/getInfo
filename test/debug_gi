#!/bin/sh

version=1.8.0
date=01/08/2018
script=$( basename "$0" )
output="$script.md"
ligneRapport="Rapport du $(date '+%d/%m/%Y %H:%M %z')  ◇  $(uname -n)  ◇  $0 $*  ◇  $version"
echo "$date" > /dev/null
code="\`\`\`"


echo
if [ "$1" = "--help" ] || [ "$1" = "-h" ] ; then
	echo "$script [options]"
	echo
	echo "  -h --help : cet affichage"
	echo "  -n --noexport : pas d'export sur un paste, juste rapport local ($script.md)"
	echo
	echo "curl requis"
	echo
	exit
fi

if ! command -v curl >/dev/null ; then
	echo "Erreur critique: curl requis mais non installé"
	if command -v "dpkg" >/dev/null ; then
		echo "Installer avec:  apt install curl"
	fi
	echo
	noexport="yes"
fi

	# $1=cmd, si cmd existe, commence un ligne $1: sinon affiche $1 ko avec return 1
get_cmd(){

	if command -v "$1" >/dev/null; then
		printf '* %s : ' "$1"
	else
		echo "* $1 **KO**"
		return 1
	fi
}

	# $1 command pour afficher version(1ère ligne), si $2=error noDisplay, erreur, si $2=stderr stderr>null, $1 en erreur, affiche n/a 
get_version(){

	if [ -z "$2" ] && eval "$1" 1>/dev/null 2>&1; then
		eval "$1" | head -n1 
	elif [ "$2" = error ] && eval "$1" 1>/dev/null 2>&1; then
		eval "$1" 2>/dev/null | head -n1 
	elif [ "$2" = stderr ] && eval "$1" 1>/dev/null 2>&1; then
		eval "$1" 2>&1 | head -n1 
	else
		echo "version n/a"
	fi
}

if [ "$1" = "--noexport" ] || [ "$1" = "-n" ] ; then
	noexport="yes"
fi

exec 3>&1			# sauvegarde
exec 1>"$output"	# redirection stdout dans fichier
printf "%s\\n\\n\\n" "$ligneRapport"

printf "\\n%s\\n" $code
grep -rA0 '.' /etc/*-release | head -n1
cat /etc/*version
printf "\\n%s\\n" $code

echo
echo "système, portabilité"	# système, portabilité
echo
get_cmd "acpi"		&& get_version "acpi --version" 
get_cmd "awk"		&& get_version "awk -V" 
get_cmd "mawk"		&& get_version "mawk -W version" error	# error: redirection stderr > null
get_cmd "bash" 		&& get_version "bash --version" 
get_cmd "dig"		&& get_version "dig -v" stderr			# stderr: redirection stderr > stdout
get_cmd "drill"		&& get_version "drill -v" stderr
get_cmd "free" 		&& get_version "free -V" 
get_cmd "grep" 		&& get_version "grep -V" 
get_cmd "host"		&& get_version "host -V" stderr
get_cmd "journalctl"	&& get_version "journalctl --version" 
get_cmd "lsblk"		&& get_version "lsblk -V" 
get_cmd "lspci"		&& get_version "lspci --version" 
if lspci -nnv 1>/dev/null 2>&1 ; then 
	echo "* lspci -nnv : ok"
else
	echo "* lspci -nnv **KO**"
	flag_lspci="ko"
fi
[ -e '/etc/shells' ] && echo "* /etc/shells : existe" || echo "* /etc/shells **KO**"
get_cmd "sed"		&& get_version "sed --version" 
get_cmd "upower"	&& get_version "upower -v" 
get_cmd "uptime" 	&& get_version "uptime -V" 
get_cmd "xargs"		&& get_version "xargs --version" 

echo
echo "video, xorg"	# video, xorg
echo
get_cmd "glxinfo"	&& echo 'ok'
get_cmd "xdpyinfo"			&& get_version "xdpyinfo -version" 
get_cmd "xdriinfo"			&& get_version "xdriinfo nscreens" && echo
get_cmd "xprop"		&& echo 'ok'
get_cmd "xrandr"			&& get_version "xrandr --version" && echo
get_cmd "xset"				&& get_version "xset -version" 
get_cmd "x-window-manager"	&& get_version "x-window-manager -V" 

echo
echo "---"
echo

###############################################################################################################

	# cpuinfo
echo '## cpuinfo'
printf "\\n%s\\n" $code
if [ -e "/proc/cpuinfo"  ]; then
	if command -v awk >/dev/null; then
		awk '{if ( $0 ~ /^$/ ) {exit} else {print}}' /proc/cpuinfo 2>/dev/null
	else
		echo "/proc/cpuinfo existe, mais pas de awk"
	fi
else
	echo "/proc/cpuinfo inexistant"
fi
printf "\\n%s\\n" $code

	# upower
if upower --version 1>/dev/null 2>&1 ; then
	echo "## upower"
	printf "\\n%s\\n" $code
	upower --enumerate
	upower --version || echo "upower KO"
	printf "\\n%s\\n" $code
fi

	# xrandr
if xrandr --version; then
	echo "## xrandr --listproviders"
	printf "\\n%s\\n" $code
	xrandr --listproviders
	printf "\\n%s\\n" $code
fi

	# xdriinfo Direct Rendering Interface?
if xdriinfo nscreens 1>/dev/null 2>&1 ; then
	echo "## $( xdriinfo -version 2>/dev/null )"
	printf "\\n%s\\n" $code
	echo "xdriinfo: $( xdriinfo 2>/dev/null )"
	echo "xdriinfo nscreens: $( xdriinfo nscreens 2>/dev/null )"
	echo "xdriinfo driver 0: $( xdriinfo driver 0 2>/dev/null )"
	printf "\\n%s\\n" $code
fi

	# xprop
if command -v xprop >/dev/null 2>&1; then
		# wm / compositor
	id=$( xprop -root -notype _NET_SUPPORTING_WM_CHECK 2>/dev/null )
	id=${id##* }
	wm_brut=$( xprop -id "$id" -notype -len 100 2>/dev/null )
		# wm annexe
	xprop="$( xprop -root -notype -display "$DISPLAY" 2>/dev/null )"
	echo "## xprop"
	printf "\\n%s\\n" $code
	echo "### xprop -id (wm_brut) :"
	echo "$wm_brut"
	echo
	echo "### xprop -root -notype -display $DISPLAY :"
	echo "$xprop"
	printf "\\n%s\\n" $code
fi

	# /usr/share/xsessions 
echo "## ls -l /usr/share/xsessions "
	printf "\\n%s\\n" $code
ls -l /usr/share/xsessions 2>/dev/null
	printf "\\n%s\\n" $code

	# init
echo "## ps -ef | head -n 2"
printf "\\n%s\\n" $code
ps -ef | head -n 2
printf "\\n%s\\n" $code

	# général
echo "## ps -e"
printf "\\n%s\\n" $code
ps -e
printf "\\n%s\\n" $code

	# env
echo "## env"
printf "\\n%s\\n" $code
env | sort
printf "\\n%s\\n" $code

if [ -z "$flag_lspci" ]; then
		# lspci
	echo "## lspci -nnv"
	printf "\\n%s\\n" $code
	lspci -nnv
	printf "\\n%s\\n" $code
fi

exec 1>&3		# restauration
exec 3>&-		# fermeture FD

if [ -z "$noexport" ]; then
	printf "%s \\n\\n" "url du rapport:"
	curl -s --upload-file "$output" "https://transfer.sh/$output"
	echo
fi

echo "voir le rapport généré: pager $output"	# output="$script.md"
echo "effacer le rapport généré: rm $output"
echo

exit 0

wget -nv -O debug_gi  https://framaclic.org/h/debug-gi
curl -L -o debug_gi  https://framaclic.org/h/debug-gi
chmod +x debug_gi && ./debug_gi

wget -nv -O debug_gi  https://framagit.org/sdeb/getInfo/raw/master/test/debug_gi
curl -O  https://framagit.org/sdeb/getInfo/raw/master/test/debug_gi
